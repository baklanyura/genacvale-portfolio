const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

mix.js('resources/newadmindirectories/js/admin/app.js', 'public/newadmin-assets/js')
    .css('resources/newadmindirectories/css/admin/app.css', 'public/newadmin-assets/css')
    .sass('resources/newadmindirectories/sass/app.scss', 'public/newadmin-assets/css')
    .sourceMaps();
//
// mix.js('resources/newadmindirectories/js/admin/app.js', 'public/newadmin-assets/js')
//
//     .sourceMaps();
