<?php

return
    [
        'contacts_location' => 'Contact and Location',
        'get_touch' => 'Get in Touch',
        'location' => 'Our Location',
        'street' => '08132, Promyslova St. 5, Vyshneve',
        'oblast' => 'Kyivs\'ka oblast, Ukraine'
    ];
