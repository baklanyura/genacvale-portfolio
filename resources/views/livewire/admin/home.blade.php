<!DOCTYPE html>
<html>
<head>
    <title></title>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @livewireStyles
</head>
<body>
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-3">
            <a href="{{ route('categories') }}">Categories</a><br>
            <a href="{{ route('products') }}">Products</a>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h2>Admin Panel - genacvale.eu</h2>
                </div>
                <div class="card-body">
                    @if (session()->has('message'))
                        <div class="alert alert-success">
                            {{ session('message') }}
                        </div>
                    @endif
                    {{ $slot }}
                </div>
            </div>
        </div>
    </div>
</div>
@livewireScripts
<script type="text/javascript">
    window.livewire.on('itemStore', () => {
        $('#exampleModal').modal('hide');
    });
</script>
</body>
</html>
