<!-- Modal -->
<div wire:ignore.self class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Редагувати Товар</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="form-group mt-3 col-md-4">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" name="title" id="title" placeholder="Enter product title" wire:model="title_en" autofocus>
                            @error('title') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group mt-3 col-md-4">
                            <label for="title">Наименование</label>
                            <input type="text" class="form-control" name="title" id="title" placeholder="Введите наименование товара" wire:model="title_ru" autofocus>
                            @error('title') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group mt-3 col-md-4">
                            <label for="title">Найменування</label>
                            <input type="text" class="form-control" name="title" id="title" placeholder="Введіть найменування товару" wire:model="title_uk" autofocus>
                            @error('title') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="category">Category</label>
                            <select wire:model="category_id" class="form-control">
                                <option value="" selected>Choose Category</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->cat_title_en }}</option>
                                @endforeach
                            </select>
                            @error('category_id') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="image">Image</label>
                            <input type="file" class="form-control" name="image" id="image" wire:model="image" placeholder="Enter image">
                            @error('image') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" id="description" wire:model="description_en" placeholder="Enter description" rows="10"></textarea>
                            @error('description') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="description">Описание</label>
                            <textarea class="form-control" name="description" id="description" wire:model="description_ru" placeholder="Введите описание" rows="10"></textarea>
                            @error('description') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label for="description">Опис</label>
                            <textarea class="form-control" name="description" id="description" wire:model="description_uk" placeholder="Введіть опис" rows="10"></textarea>
                            @error('description') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancel()" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                <button type="button" wire:click.prevent="update()" class="btn btn-primary" data-dismiss="modal">Зберегти зміни</button>
            </div>
        </div>
    </div>
</div>
