@section('breadcrumb-active-page', 'Товари')
<div>
    @include('livewire.admin.products.create')
    @include('livewire.admin.products.update')
{{--    @if (session()->has('message'))--}}
{{--        <div class="alert alert-success" style="margin-top:30px;">x--}}
{{--            {{ session('message') }}--}}
{{--        </div>--}}
{{--    @endif--}}
    <table class="table table-bordered mt-5">
        <thead>
        <tr>
            <th>No.</th>
            <th>Найменування</th>
            <th>Зображення</th>
            <th>Опис</th>
            <th>Категорія</th>
            <th>Дії</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->title_en }}</td>
                <td><img src="{{ asset('storage/' . $value->image) }}" alt="{{ $value->title }}" width="40"></td>
                <td>{{ $value->description_en }}</td>
                <td>{{ $value->category->cat_title_en }}</td>
                <td>
                    <button data-toggle="modal" data-target="#updateModal" wire:click="edit({{ $value->id }})" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></button>
                    <button wire:click="delete({{ $value->id }})" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
