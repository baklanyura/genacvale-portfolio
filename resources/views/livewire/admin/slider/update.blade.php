<!-- Modal -->
<div wire:ignore.self class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" name="title" id="title" placeholder="Enter product title" wire:model="title" autofocus>
                                @error('title') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>

                            <div class="form-group">
                                <label for="price">Price</label>
                                <input type="number" class="form-control" name="price" id="price" wire:model="price" placeholder="Enter price">
                                @error('price') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="category">Category</label>
                                <select wire:model="category_id" class="form-control">
                                    <option value="" selected>Choose Category</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->cat_title }}</option>
                                    @endforeach
                                </select>
                                @error('category_id') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>


                            <div class="form-group">
                                <label for="weight">Weight</label>
                                <input type="number" class="form-control" name="weight" id="weight" wire:model="weight" placeholder="Enter weight">
                                @error('weight') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" class="form-control" name="image" id="image" wire:model="image" placeholder="Enter description">
                        @error('image') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="short_description">Short Description</label>
                        <input type="text" class="form-control" name="short_description" id="short_description" wire:model="short_description" placeholder="Enter short description">
                        @error('short_description') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" name="description" id="description" wire:model="description"></textarea>
                        @error('description') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancel()" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" wire:click.prevent="update()" class="btn btn-primary" data-dismiss="modal">Save changes</button>
            </div>
        </div>
    </div>
</div>
