<div>
    @include('livewire.admin.products.create')
    @include('livewire.admin.products.update')
{{--    @if (session()->has('message'))--}}
{{--        <div class="alert alert-success" style="margin-top:30px;">x--}}
{{--            {{ session('message') }}--}}
{{--        </div>--}}
{{--    @endif--}}
    <table class="table table-bordered mt-5">
        <thead>
        <tr>
            <th>No.</th>
            <th>Title</th>
            <th>Image</th>
            <th>Short Description</th>
            <th>Description</th>
            <th>Category</th>
            <th>Price</th>
            <th>Weight</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->title }}</td>
                <td><img src="{{ asset('storage/' . $value->image) }}" alt="{{ $value->title }}" width="40"></td>
                <td>{{ $value->short_description }}</td>
                <td>{{ $value->description }}</td>
                <td>{{ $value->category->cat_title }}</td>
                <td>{{ $value->price }}</td>
                <td>{{ $value->weight }}</td>
                <td>
                    <button data-toggle="modal" data-target="#updateModal" wire:click="edit({{ $value->id }})" class="btn btn-primary btn-sm">Edit</button>
                    <button wire:click="delete({{ $value->id }})" class="btn btn-danger btn-sm">Delete</button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
