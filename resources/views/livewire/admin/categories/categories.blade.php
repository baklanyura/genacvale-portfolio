@section('breadcrumb-active-page', 'Категорії')
<div>
    @include('livewire.admin.categories.create')
    @include('livewire.admin.categories.update')

    <table class="table table-bordered mt-5">
        <thead>
        <tr>
            <th>No.</th>
            <th>Category Title</th>
            <th>Назва Категорії</th>
            <th>Название Категории</th>
            <th>Дії</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->cat_title_en }}</td>
                <td>{{ $value->cat_title_uk }}</td>
                <td>{{ $value->cat_title_ru }}</td>
                <td>
                    <button data-toggle="modal" data-target="#updateModal" wire:click="edit({{ $value->id }})" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></button>
                    <button wire:click="delete({{ $value->id }})" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
