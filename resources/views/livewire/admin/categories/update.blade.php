<!-- Modal -->
<div wire:ignore.self class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Редагувати Категорію</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Category Title</label>
                        <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Enter Category Title" wire:model="cat_title_en">
                        @error('cat_title') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Назва Категорії</label>
                        <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Введіть Назву Категорії" wire:model="cat_title_uk">
                        @error('cat_title') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Название Категории</label>
                        <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Введите Название Категории" wire:model="cat_title_ru">
                        @error('cat_title') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancel()" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                <button type="button" wire:click.prevent="update()" class="btn btn-primary" data-dismiss="modal">Зберегти зміни</button>
            </div>
        </div>
    </div>
</div>
