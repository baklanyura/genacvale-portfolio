<div>
    <input
        type="text"
        class="form-control"
        wire:model="query"
        wire:keydown.escape="empty"
        wire:keydown.tab="empty"
    />
    <input type="hidden" name="customer_id" value="{{ $select }}">
    <div> @error('name'){{ $message }}@enderror </div>
    @if(!empty($query))
        @if(count($customers) > 0)
            <select class="form-control" data-placeholder="Выберите клиента" aria-hidden="true"  id="customer_id" size="{{ count($customers) }}" style="z-index: 1" wire:model="select" wire:click="selection">
                @foreach($customers as $customer)
                    <option value="{{ $customer->id }}" >{{ $customer->title }} | {{ $customer->address }} @if($customer->legal_status), <span>{{ $customer->legal_status }}</span> @endif</option>
                @endforeach
            </select>
            @error('customer_id')<span class="text-danger"><small>{{ $message }}</small></span>@enderror
        @else
            @if(Str::length($query) < 15)
                <div>Клиентов не найдено</div>
            @endif
        @endif
    @endif
</div>
