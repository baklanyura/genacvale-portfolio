
@yield('class')
    <div class="container">
        <h1>@yield('pageTitle')</h1>
        <h3>@yield('pageSlogan')</h3>
    </div>
</section>


