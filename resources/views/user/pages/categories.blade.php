@extends('user.layouts.app')
@extends('user.layouts.page-banner-section')
@section('pageTitle', 'Our Collection')
@section('pageSlogan', 'THE PREMIUM FOOD EXPERIENCE')
@section('mainContent')
   @include('user.includes.categoriesPageInc.menu-section')

@endsection

@section('title', 'Our Collection')
@section('class')
    <section class="page-banner-section menu">
@endsection
