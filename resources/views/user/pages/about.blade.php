@extends('user.layouts.app')
@extends('user.layouts.page-banner-section')

@section('mainContent')
    @include('user.includes.aboutPageInc.about')
    @include('user.includes.aboutPageInc.statisticSection')
@endsection

@section('pageTitle',  __('about.about_us') )
@section('pageSlogan', __('about.our_history'))

@section('title', 'About Us')

@section('class')
    <section class="page-banner-section about">
@endsection


