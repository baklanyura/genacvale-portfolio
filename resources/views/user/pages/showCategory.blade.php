@extends('user.layouts.app')
@extends('user.layouts.page-banner-section')
@section('pageTitle', $category->{'cat_title_' . app()->getLocale()})


@section('pageSlogan', 'THE PREMIUM FOOD EXPERIENCE')
@section('mainContent')
    @include('user.includes.categoriesPageInc.showCateforySection')
@endsection

@section('title', $category->cat_slug)
@section('class')
    @if($category->cat_slug == 'sauces')
        <section class="page-banner-section menu souses">
    @elseif($category->cat_slug == 'seasoning')
        <section class="page-banner-section menu seasoning">
    @elseif($category->cat_slug == 'grocery')
        <section class="page-banner-section menu grocery">
    @else
        <section class="page-banner-section menu">
    @endif

@endsection
