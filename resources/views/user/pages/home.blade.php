@extends('user.layouts.app')
@section('mainContent')
    @include('user.includes.homePageInc.homeSection')
    @include('user.includes.homePageInc.popular-menu')
    @include('user.includes.homePageInc.contact-section')

@endsection
@section('title', 'Home')
