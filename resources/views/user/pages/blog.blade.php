@extends('user.layouts.app')
@extends('user.layouts.page-banner-section')
@section('pageTitle', __('blog.our_blog'))
@section('pageSlogan', __('blog.news'))
@section('mainContent')
  @include('user.includes.blogPageInc.blog-section')
@endsection



@section('title', 'Blog')
@section('class')
    <section class="page-banner-section blog">
@endsection
