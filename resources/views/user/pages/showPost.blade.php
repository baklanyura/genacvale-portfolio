@extends('user.layouts.app')
@extends('user.layouts.page-banner-section')
@section('pageTitle'){{ $post->{ 'title_' . app()->getLocale() } }} @endsection
@section('pageSlogan') {{ $post->{ 'subtitle_' . app()->getLocale() } }} @endsection
@section('mainContent')

    @include('user.includes.blogPageInc.show-post-section')
    @include('user.includes.blogPageInc.comments')
        </div>
    </div>
</section>
@endsection

@section('title'){{ $post->{ 'title_' . app()->getLocale() } }} @endsection
@section('class')
    <section class="page-banner-section blog">
@endsection
