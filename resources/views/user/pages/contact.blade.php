@extends('user.layouts.app')
@extends('user.layouts.page-banner-section')
@section('pageTitle', __('contacts.contacts_location'))
@section('pageSlogan', __('contacts.get_touch'))
@section('mainContent')
  @include('user.includes.contactPageInc.mapSection')
   @include('user.includes.contactPageInc.contact-section')

@endsection

@section('title', 'Contact and Location')
@section('class')

    <section class="page-banner-section contact">
@endsection


