<!-- about-section
    ================================================== -->
<section class="about-section">
    <div class="container">
        <div class="about-box">
            <div class="row">
                <div class="col-md-6">
                    <div class="about-content">
                        <h2 class="text-center">{{ __('about.our_history') }}</h2>
                        <p>{{ __('about.short_history_1') }} </p>
                        <p>{{ __('about.short_history_2') }}</p>
                    </div>
                </div>
                <div class="col-md-6">
{{--                   <div class="about-images">--}}
                        <video width="100%" height="auto" controls autoplay muted id="videoId">
                            <source src="{{ asset('user/videos/genacvale_videopresentation_mus2_logo top-left.mp4') }}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End about section -->
