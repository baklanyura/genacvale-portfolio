<!-- statistic-section
    ================================================== -->
<section class="statistic-section">
    <div class="container">
        <div class="statistic-box">
            <h2 class="text-center">{{ __('about.distributor1') }}</h2>
            <h4 class="mt-2">{{ __('about.distributor3') }}</h4>
            <div class="row">
                <div class="col-md-2 col-sm-4">
                    <div class="statistic-post ">
                        <a href="https://www.facebook.com/TastetheGorgia" target="_blank"><img src="{{ asset('user/images/partners/logo_g_u_group.jpg') }}" alt="zakuski"></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="d-flex flex-column">
                        <p><span>{{ __('about.phone') }}: </span> +380 99 555 1800</p>
                        <p><span>{{ __('about.email') }}: </span>info@genacvale.kiev.ua</p>
                    </div>
                </div>
            </div>
            <h4 class="mt-2">{{ __('about.distributor2') }}</h4>
            <div class="row">
                <div class="col-md-2 col-sm-4">
                    <div class="statistic-post ">
                        <a href="https://zakuski.in.ua/" target="_blank"><img src="{{ asset('user/images/partners/logo_dinastia.png') }}" alt="zakuski"></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="d-flex flex-column">
                        <p><span>{{ __('about.phone') }}: </span> +380 67 373 8668</p>
                        <p style="padding-left: 35px;">+380 67 513 3122</p>
                    </div>
                </div>
            </div>
            <br>
            <h2 class="text-center">{{ __('about.partners') }}</h2>
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="statistic-post">
                        <a href="http://www.fseafood.com/"><img src="{{ asset('user/images/partners/fseafood.png') }}" alt="fseafood"></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="statistic-post">
                        <img src="{{ asset('user/images/partners/meattest.png') }}" alt="meattest">
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="statistic-post">
                        <a href="https://myasnova.in.ua/g84776153-gotovaya-produktsiya"><img src="{{ asset('user/images/partners/meatnova.png') }}" alt="meattest"></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="statistic-post">
                        <a href="https://www.instagram.com/mersi_fresh/"><img src="{{ asset('user/images/partners/mersi.png') }}" alt="mersi"></a>

                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="statistic-post">
                        <img src="{{ asset('user/images/partners/lesik.png') }}" alt="lesik">
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="statistic-post">
                        <img src="{{ asset('user/images/partners/gourmet.png') }}" alt="gourmet">
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="statistic-post">
                        <a href="https://myasok.com.ua/"><img src="{{ asset('user/images/partners/meat.png') }}" alt="meat"></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="statistic-post">
                        <a href=" https://rizhky-ta-nizhky.com.ua/"><img src="{{ asset('user/images/partners/rizhki-nizhki.png') }}" alt="rizhki-nizhki"></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="statistic-post">
                        <a href="https://galya-baluvana.kiev.ua/"><img src="{{ asset('user/images/partners/hali-baluvana.png') }}" alt="hali-baluvana"></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="statistic-post">
                        <img src="{{ asset('user/images/partners/visit.png') }}" alt="visit">
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="statistic-post">
                        <a href="https://www.instagram.com/chefmeatkyiv/"><img src="{{ asset('user/images/partners/chef-meat.png') }}" alt="chef-meat"></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="statistic-post">
                        <a href="https://www.facebook.com/svit.miasa.brovary/"><img src="{{ asset('user/images/partners/fresh-meat.png') }}" alt="fresh-meat"></a>
                    </div>
                </div>
{{--                <div class="col-md-3 col-sm-6">--}}
{{--                    <div class="statistic-post">--}}
{{--                        <a href="https://badyorui.com.ua/shops"><img src="{{ asset('user/images/partners/badiorij.png') }}" alt="badiorij"></a>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="col-md-3 col-sm-6">
                    <div class="statistic-post">
                        <a href="http://www.evrotek.com/ua/fresh/"><img src="{{ asset('user/images/partners/fresh.png') }}" alt="fresh"></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="statistic-post">
                        <a href="https://ukrmyso.all.biz/uk/"><img src="{{ asset('user/images/partners/ukr-meat.png') }}" alt="ukr-meat"></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="statistic-post">
                        <a href="https://winegallery.com.ua/"><img src="{{ asset('user/images/partners/wine.png') }}" alt="wine"></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="statistic-post">
                        <img src="{{ asset('user/images/partners/italia-del-gusto.png') }}" alt="italia-del-gusto">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End statistic-section -->
