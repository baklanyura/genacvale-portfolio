<!-- menu-section
			================================================== -->
@foreach($categories as $value)
<section class="menu-section {{ $loop->odd ? 'right-content' : 'left-content' }} ">
    <div class="background-items">
        <div class="table-back"></div>
    </div>
    <div class="menu-box">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="title-section {{ $loop->odd ? '' : 'white-style'}}">
                        <h1>{{ $value->{'cat_title_' . app()->getLocale()} }}</h1>
                    </div>
                    <ul class="menu-list-items">
                        @if(count($value->products)>0)
                            @foreach($value->products as $product)
                            <li class="list-item">
                                <div class="list-content">
                                    <h2>{{ $product->{'title_' . app()->getLocale()} }}</h2>
                                    <p>{{ $product->{'description_' .  app()->getLocale()} }}</p>

                                </div>
                            </li>
                            @endforeach
                        @else
                            <li class="list-item">
                                <div class="list-content">
                                    <h2>There are no products in this category.</h2>
                                </div>
                             </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@endforeach
<!-- End menu section -->
