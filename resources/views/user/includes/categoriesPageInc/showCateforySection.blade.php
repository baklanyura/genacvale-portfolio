<section class="about-section">
    @if(count($category->products)>0)
        @foreach($category->products as $product)
            <div class="container">
                 <div class="about-box">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="about-content">
                                <h2>{{ $product->{'title_' . app()->getLocale()} }}</h2>
                                <p>{{ $product->{ 'description_' . app()->getLocale() } }}</p>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="about-images">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="image">
                                            <img src="{{ asset('storage/' . $product->image) }}" alt="">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @else
    <h1 class="text-dark text-center">{{ __('categories.absent') }}</h1>
    @endif
</section>
