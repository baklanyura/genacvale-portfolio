<!-- today-menu-section
			================================================== -->
<section class="today-menu-section">
    <div class="container">
        <div class="center-area">
            <h2>Today's Special</h2>
        </div>
        <div class="menu-box">
            <div class="owl-wrapper">
                <div class="owl-carousel" data-num="3">
                    @foreach($categories as $value)
                    <div class="item">
                        <div class="menu-post">
                            <img src="{{ asset('user/images/menuPage/adjika.jpg') }}" alt="">
                            <div class="menu-post-content">
                                <div class="inner-menu">
                                    <h2>{{ $value->cat_title }}</h2>
                                    <span>{{ $value->cat_slug }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End today-menu section -->
