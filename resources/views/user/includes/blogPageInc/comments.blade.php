<div class="comment-area-box">
    <div class="center-area">

        <h2>{{ $post->comments->count() }} Comments</h2>
    </div>
    <ul class="comment-tree">
                @comments(['model' => $post])
    </ul>
</div>
