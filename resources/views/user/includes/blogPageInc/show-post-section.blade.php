<!-- blog-section ================================================== -->
<section class="blog-section single-post">
    <div class="container">
        <div class="blog-box">
            <div class="blog-post">
                <img src="{{ asset('storage/' . $post->image) }}" alt="">
                <p>{!! htmlspecialchars_decode($post->{'body_' . app()->getLocale()}) !!}</p>
                <ul class="share-post text-center">
                    <li><a class="facebook" href="https://www.facebook.com/TastetheGorgia"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="instagram" href="https://www.instagram.com/g.u.group/?fbclid=IwAR3RWSKXe2stAQUSQaZhF32D6hCY8LG-aXTm60X_L8crnci_c_lkKMXlXaM"><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>
