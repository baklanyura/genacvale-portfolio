<!-- blog-section
			================================================== -->
<section class="blog-section">
    <div class="container">
        <div class="blog-box">
            @foreach($posts as $post)
            <div class="blog-post">
                <img src="{{ asset('storage/' . $post->image) }}" alt="">
                <h2><a href="{{ route('user.showPost', $post->id) }} ">{{ $post->{'title_' . app()->getLocale()} }}</a></h2>
                <ul class="post-meta">
                    <li>
                        <a href="#">{{ $post->created_at }}</a>
                    </li>
                    <li>
                        <a href="#">Food</a>
                    </li>
                    <li>
                       <a href="{{ route('user.showPost', $post->id) }}">{{ $post->comments->count() }} comments</a>
                    </li>
                </ul>
                <p>{{ $post->{'subtitle_' . app()->getLocale()} }}</p>
                <ul class="share-post">
                    <li><a class="facebook" href="https://www.facebook.com/TastetheGorgia"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="instagram" href="https://www.instagram.com/g.u.group/?fbclid=IwAR3RWSKXe2stAQUSQaZhF32D6hCY8LG-aXTm60X_L8crnci_c_lkKMXlXaM"><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>
            @endforeach
            <div class="pagination-box blog-pagination">
{{--                <ul class="page-list">--}}
                   {{ $posts->links() }}
{{--                    <li>  <a class="prev" href="#"><i class="fa fa-angle-left"></i>{{ __('blog.prev') }}</a></li>--}}
{{--                    <li><a class="active" href="#">1</a></li>--}}
{{--                    <li><a href="#">2</a></li>--}}
{{--                    <li><a href="#">3</a></li>--}}
{{--                    <li><a class="next" href="#">{{ __('blog.next') }} <i class="fa fa-angle-right"></i></a></li>--}}
{{--                </ul>--}}
            </div>
        </div>
    </div>
</section>
<!-- End blog section -->
