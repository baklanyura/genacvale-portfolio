<!-- contact-section
        ================================================== -->
<section class="contact-section" id="contact-section">
    <div class="container">
        <div class="title-section white-style">
            <h1>{{ __('home.contact_with_us') }}</h1>
        </div>
        <form action="{{ route('user.send') }}" method="POST" id="contact-form">
            @if(Session::has('Message_sent'))
                <div class="alert alert-success" role="alert">
                    {{ Session::get('Message_sent') }}
                </div>
            @endif
            @csrf
            <div class="row">
                <div class="col-sm-6">
                    <input name="name" id="name" type="text" placeholder="{{ __('home.placeholder_name') }}" value="{{ old('name') }}">
                    @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-sm-6">
                    <input name="mail" id="mail" type="text" placeholder="{{ __('home.placeholder_email') }}" value="{{ old('mail') }}">
                    @error('mail')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-sm-6">
                    <input name="subject" id="subject" type="text" placeholder="{{ __('home.placeholder_subject') }}" value="{{ old('subject') }}">
                    @error('subject')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-sm-6">
                    <input name="phone" id="phone" type="text" placeholder="{{ __('home.placeholder_phone') }}" value="{{ old('phone') }}">
                    @error('phone')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
                @error('comment')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            <textarea name="comment" id="comment" placeholder="{{ __('home.placeholder_message') }}">{{ old('comment') }}</textarea>
            <div class="center-area">
                <button type="submit">{{ __('home.input_value_send_message') }}</button>
            </div>
            <div id="msg" class="message alert">

            </div>
        </form>
        <div class="contact-info">
            <div class="row">
                <div class="col-md-4">
                    <div class="info-box">
                        <span aria-hidden="true" class="icon_pin_alt"></span>
                        <h2>{{ __('home.our_location') }}</h2>
                        <p>{{ __('contacts.street') }} <br> {{ __('contacts.oblast') }} </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info-box">
                        <span aria-hidden="true" class="icon_mobile"></span>
                        <h2>{{ __('home.our_phones') }}</h2>
                        <p>+38-099-555-18-00 </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info-box">
                        <span aria-hidden="true" class="icon_mail_alt"></span>
                        <h2>{{ __('home.our_mail') }}</h2>
                        <p>info@Genacvale.kiev.ua </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End contact section -->
