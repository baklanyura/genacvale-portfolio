<!-- popular-menu-section
        ================================================== -->
<section class="popular-menu-section">
    <div class="container">
        <div class="title-section white-style">
            <h1>{{ __('home.popgoods') }}</h1>
        </div>
        <div class="menu-box owl-wrapper">
            <div class="owl-carousel" data-num="2">

                    @foreach($products as $value)
                        <div class="item">
                            <div class="menu-post {{ $loop->odd ? '' : 'right-image'}}">
                                <img src="{{ asset('storage/' . $value->image) }}" alt="{{ $value->{ 'title_' . app()->getLocale() } }}">
                                <div class="menu-post-content">
                                    <h2>{{ $value->{'title_' . app()->getLocale()} }}</h2>
                                    <p>{{ Str::of($value->{ 'description_' . app()->getLocale() }) }}</p>

                                </div>
                            </div>
                        </div>
                    @endforeach

            </div>
        </div>
    </div>
</section>
<!-- End popular-menu section -->
