<!-- home-section
        ================================================== -->
<section id="home-section">
    <div id="rev_slider_202_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="concept1" style="background-color:#000000;padding:0px;">
        <!-- START REVOLUTION SLIDER 5.1.1RC fullscreen mode -->
        <div id="rev_slider_202_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.1.1RC">
            <ul>
                <!-- SLIDE  -->
                <li data-index="rs-672" data-transition="slidingoverlaydown" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="upload/others/slide1.jpg" data-rotate="0" data-saveperformance="off" data-title="unique" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('user/images/topPhoto/2008x750_souces.svg') }}" alt="" data-bgposition="center center" data-bgfit="contain" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption Concept-Title tp-resizeme rs-parallaxlevel-2"
                         id="slide-672-layer-2"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-fontsize="['72','70','40','30']"
                         data-lineheight="['80','80','50','36']"
                         data-voffset="['0','0','0','0']"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                         data-start="1400"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 7; white-space: nowrap;text-align:center;">{{ __('home.slider_best_select') }}
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption Concept-SubTitle tp-resizeme rs-parallaxlevel-2"
                         id="slide-672-layer-1"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['90','90','70','60']"
                         data-fontsize="['14','14','13','12']"
                         data-lineheight="['24','24','22','22']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                         data-start="1800"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 6; white-space: nowrap;font-style:italic;">{{ __('home.slider_fresh_ingr') }}
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption Concept-SubTitle tp-resizeme rs-parallaxlevel-2"
                         id="slide-672-layer-3"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['170','170','130','110']"
                         data-fontsize="['13','13','11','11']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                         data-start="2300"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 6; white-space: nowrap;font-style:italic;"><a class="button-one" id="button-scroll" rel="m_PageScroll2id" href="#contact-section">{{ __('home.write_to_us') }}</a>
                    </div>

                </li>
                <!-- SLIDE  -->
                <li data-index="rs-674" data-transition="slidingoverlayright" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="upload/others/slide2.jpg" data-rotate="0" data-saveperformance="off" data-title="professional" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('user/images/topPhoto/2008x750_souces.svg') }}" alt="" data-bgposition="center center" data-bgfit="contain" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina width="800">
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption Concept-SubTitle tp-resizeme rs-parallaxlevel-2"
                         id="slide-674-layer-1"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['-90','-90','-70','-50']"
                         data-fontsize="['18','18','16','16']"
                         data-lineheight="['24','24','22','22']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                         data-start="1400"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 6; white-space: nowrap;font-style:italic;">{{ __('home.slider_discover_spices') }}
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption Concept-Title tp-resizeme rs-parallaxlevel-2"
                         id="slide-674-layer-2"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-fontsize="['72','70','40','30']"
                         data-lineheight="['80','80','50','36']"
                         data-voffset="['20','20','0','0']"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                         data-start="1800"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 7; white-space: nowrap;text-align:center;">{{ __('home.slider_natinnovations') }} <br>{{ __('home.slider_projects') }}
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption Concept-SubTitle tp-resizeme rs-parallaxlevel-2"
                         id="slide-674-layer-3"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['160','155','105','95']"
                         data-fontsize="['13','13','11','11']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                         data-start="2300"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 6; white-space: nowrap;font-style:italic;"><a class="button-one" href="{{ route('user.blog') }}">{{ __('home.btn_read_more') }}</a>
                    </div>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-675" data-transition="slidingoverlayup" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="upload/others/slide3.jpg" data-rotate="0" data-saveperformance="off" data-title="ideas" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('user/images/topPhoto/2008x748_souces2.svg') }}" alt="" data-bgposition="center center" data-bgfit="contain" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption Concept-SubTitle tp-resizeme rs-parallaxlevel-2"
                         id="slide-675-layer-1"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['-90','-90','-70','-50']"
                         data-fontsize="['18','18','16','16']"
                         data-lineheight="['24','24','22','22']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                         data-start="1400"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 6; white-space: nowrap;font-style:italic;">{{ __('home.slider_discover_spices') }}
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption Concept-Title tp-resizeme rs-parallaxlevel-2"
                         id="slide-675-layer-2"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-fontsize="['72','70','40','30']"
                         data-lineheight="['80','80','50','36']"
                         data-voffset="['20','20','0','0']"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                         data-start="1800"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 7; white-space: nowrap;text-align:center;">{{ __('home.slider_we_agency') }}<br>{{ __('home.slider_creative') }}
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption Concept-SubTitle tp-resizeme rs-parallaxlevel-2"
                         id="slide-675-layer-3"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['160','155','105','95']"
                         data-fontsize="['13','13','11','11']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                         data-start="2300"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 6; white-space: nowrap;font-style:italic;"><a class="button-one" href="{{ route('user.about') }}">{{ __('home.btn_read_more') }}</a>
                    </div>

                </li>
                <!-- SLIDE  -->
                <li data-index="rs-672" data-transition="slidingoverlaydown" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="upload/others/slide1.jpg" data-rotate="0" data-saveperformance="off" data-title="unique" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('user/images/topPhoto/2008x748_souces2.svg') }}" alt="" data-bgposition="center center" data-bgfit="contain" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption Concept-Title tp-resizeme rs-parallaxlevel-2"
                         id="slide-672-layer-2"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-fontsize="['72','70','40','30']"
                         data-lineheight="['80','80','50','36']"
                         data-voffset="['0','0','0','0']"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                         data-start="1400"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 7; white-space: nowrap;text-align:center;">{{ __('home.slider_best_select') }}
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption Concept-SubTitle tp-resizeme rs-parallaxlevel-2"
                         id="slide-672-layer-1"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['90','90','70','60']"
                         data-fontsize="['14','14','13','12']"
                         data-lineheight="['24','24','22','22']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                         data-start="1800"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 6; white-space: nowrap;font-style:italic;">{{ __('home.slider_fresh_ingr') }}
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption Concept-SubTitle tp-resizeme rs-parallaxlevel-2"
                         id="slide-672-layer-3"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['170','170','130','110']"
                         data-fontsize="['13','13','11','11']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                         data-start="2300"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 6; white-space: nowrap;font-style:italic;"><a class="button-one" id="button-scroll" rel="m_PageScroll2id" data-ps2id-offset="100" href="#contact-section">{{ __('home.see_our_collection') }}</a>
                    </div>

                </li>
                <!-- SLIDE  -->
                <li data-index="rs-674" data-transition="slidingoverlayright" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="upload/others/slide2.jpg" data-rotate="0" data-saveperformance="off" data-title="professional" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('user/images/topPhoto/1972x710_tkemali.svg') }}" alt="" data-bgposition="center center" data-bgfit="contain" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption Concept-SubTitle tp-resizeme rs-parallaxlevel-2"
                         id="slide-674-layer-1"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['-90','-90','-70','-50']"
                         data-fontsize="['18','18','16','16']"
                         data-lineheight="['24','24','22','22']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                         data-start="1400"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 6; white-space: nowrap;font-style:italic;">{{ __('home.slider_discover_spices') }}
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption Concept-Title tp-resizeme rs-parallaxlevel-2"
                         id="slide-674-layer-2"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-fontsize="['72','70','40','30']"
                         data-lineheight="['80','80','50','36']"
                         data-voffset="['20','20','0','0']"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                         data-start="1800"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 7; white-space: nowrap;text-align:center;">{{ __('home.slider_natinnovations') }} <br>{{ __('home.slider_projects') }}
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption Concept-SubTitle tp-resizeme rs-parallaxlevel-2"
                         id="slide-674-layer-3"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['160','155','105','95']"
                         data-fontsize="['13','13','11','11']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                         data-start="2300"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 6; white-space: nowrap;font-style:italic;"><a class="button-one" href="{{ route('user.blog') }}">{{ __('home.btn_read_more') }}</a>
                    </div>
                </li>
                <li data-index="rs-674" data-transition="slidingoverlayright" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="upload/others/slide2.jpg" data-rotate="0" data-saveperformance="off" data-title="professional" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('user/images/topPhoto/2008x748_souces2.svg') }}" alt="" data-bgposition="center center" data-bgfit="contain" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption Concept-SubTitle tp-resizeme rs-parallaxlevel-2"
                         id="slide-674-layer-1"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['-90','-90','-70','-50']"
                         data-fontsize="['18','18','16','16']"
                         data-lineheight="['24','24','22','22']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                         data-start="1400"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 6; white-space: nowrap;font-style:italic;">{{ __('home.slider_discover_spices') }}
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption Concept-Title tp-resizeme rs-parallaxlevel-2"
                         id="slide-674-layer-2"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-fontsize="['72','70','40','30']"
                         data-lineheight="['80','80','50','36']"
                         data-voffset="['20','20','0','0']"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                         data-start="1800"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 7; white-space: nowrap;text-align:center;">{{ __('home.slider_natinnovations') }} <br>{{ __('home.slider_projects') }}
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption Concept-SubTitle tp-resizeme rs-parallaxlevel-2"
                         id="slide-674-layer-3"
                         data-x="['center','center','center','center']"
                         data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']"
                         data-voffset="['160','155','105','95']"
                         data-fontsize="['13','13','11','11']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"
                         data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                         data-start="2300"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         style="z-index: 6; white-space: nowrap;font-style:italic;"><a class="button-one" href="{{ route('user.blog') }}">{{ __('home.btn_read_more') }}</a>
                    </div>
                </li>

            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>
    <!-- END REVOLUTION SLIDER -->
</section>
<!-- End home section -->
