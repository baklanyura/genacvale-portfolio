<!doctype html>


<html lang="en" class="no-js">
<head>
    <title>@yield('title')</title>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="{{ asset('user/css/restory-assets.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/style.css') }}">
    <script src="{{ asset('user/js/restory-assets.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('user/js/script.js') }}"></script>
    <script type="text/javascript" src="{{ asset('user/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('user/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('user/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('user/js/extensions/revolution.extension.parallax.min.js') }}"></script>

</head>
<body>

<!-- Container -->
<div id="container">
