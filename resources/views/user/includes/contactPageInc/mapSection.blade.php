<!-- map-section
        ================================================== -->
<div class="flex-center text-center position-ref full-height">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="content map-content">
                    <h2>{{ __('contacts.location') }}</h2>
                    <div class="map" id="map">
                        @if(app()->getLocale() == 'en')
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d5087.907276048712!2d30.3381183!3d50.3860628!3m2!1i1024!2i768!4f13.
            1!3m3!1m2!1s0x40d4ca4759eb2a21%3A0x34c77d56472f1628!2sPromyslova%20St%2C%205%2C%20Vyshneve%2C%20Kyivs&#39;
            ka%20oblast%2C%2008132!5e0!3m2!1sen!2sua!4v1622069963369!5m2!1sen!2sua" width="90%" height="100%" style="border:0;" allowfullscreen="" loading="lazy">
                            </iframe>
                        @elseif(app()->getLocale() == 'uk')
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d5087.907276048712!2d30.3381183!3d50.3860628!3m2!1i1024!2i768!4f13.
                1!3m3!1m2!1s0x40d4ca4759eb2a21%3A0x34c77d56472f1628!2z0LLRg9C70LjRhtGPINCf0YDQvtC80LjRgdC70L7QstCwLCA1LCDQktC40YjQvdC10LLQtSwg0JrQuNGX0LLRgdGM0LrQsCDQvtCx0LsuLCAwODEzMg!
                5e0!3m2!1suk!2sua!4v1622072911424!5m2!1suk!2sua" width="90%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        @else
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d5087.907276048712!2d30.3381183!3d50.3860628!3m2!1i1024!2i768!4f13.
                1!3m3!1m2!1s0x40d4ca4759eb2a21%3A0x34c77d56472f1628!2z0YPQuy4g0J_RgNC-0LzRi9GI0LvQtdC90L3QsNGPLCA1LCDQktC40YjQvdC10LLQvtC1LCDQmtC40LXQstGB0LrQsNGPINC-
                0LHQu9Cw0YHRgtGMLCAwODEzMg!5e0!3m2!1sru!2sua!4v1622072814913!5m2!1sru!2sua" width="90%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>


