<!-- Header-->
<header class="clearfix trans">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="inner-navbar">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ route('user.home') }}">Genacvale</a>

                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right navigate-section">
                        @can('view', auth()->user())
                            <li><a href="{{ route('admin.dashboard') }}">Admin-panel</a></li>
                        @endcan
                        <li><a class="{{ Request::path() == 'home' ? 'active' : ''  }}" href="{{ route('user.home') }}">{{ __('home.home') }}</a></li>
                        <li><a class="{{ Request::path() == 'about' ? 'active' : ''  }}" href="{{ route('user.about') }}">{{ __('home.about') }}</a></li>
                        <li class="nav-item dropdown" id="categoriesItem">
                            <a class="nav-link dropdown-toggle {{ Request::has('categories') ? 'active' : ''  }}" href="#" id="dropdown01" data-bs-toggle="dropdown" aria-expanded="false">{{ __('home.categories')}}</a>
                            <ul class="dropdown-menu" aria-labelledby="dropdown01" id="categoriesList">
                                @foreach($categories as $category)
                                    <li><a class="dropdown-item" href="{{ route('user.showCategory', $category->cat_slug ) }}" >{{ $category->{'cat_title_' . app()->getLocale()} }}</a></li>
                                @endforeach
                            </ul>
                        </li>

                        <li><a class="{{ Request::path() == 'posts' ? 'active' : ''  }}" href="{{ route('user.blog') }}">{{ __('home.blog') }}</a></li>
                        <li><a class="{{ Request::path() == 'contact' ? 'active' : ''  }}" href="{{ route('user.contact') }}">{{ __('home.location') }}</a></li>
                        <li class="nav-item dropdown" id="nav-bar-lang-flag">
                            <a class="nav-link dropdown-toggle pl-4" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{ asset('user/images/flags/' . Config::get('languages')[App::getLocale()]['flag-icon'] . '.svg')}}" width=20> {{ Config::get('languages')[App::getLocale()]['display'] }}
                            </a>
                            <ul class="dropdown-menu languange-dropdown dropdown-menu-dark" aria-labelledby="navbarDropdownMenuLink" id="lang-flags">
                                @foreach (Config::get('languages') as $lang => $language)
                                    @if ($lang != App::getLocale())
                                        <li class="dropdown-item">
                                            <a class="dropdown-item" href="{{ route('lang.switch', $lang) }}">
                                                {{--                                                <span style="background-image: url({{ asset('images/' . $language['flag-icon'] . '.svg') }})"></span>--}}
                                                <img class="ml-3"
                                                     src="{{ asset('user/images/flags/' . $language['flag-icon'] . '.svg') }}"
                                                     width=20 alt="flag">
                                                {{$language['display']}}
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </div><!-- /.container -->
    </nav>
</header>
<!-- End Header -->
