<!DOCTYPE html>
<html lang="uk">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Genacvale contact form</title>
</head>
<body>
<div class="container">
    <div class="row">
        <p>Name: {{ $details['name'] }}</p>
        <p>Email: {{ $details['mail'] }}</p>
        <p>Subject: {{ $details['subject'] }}</p>
        <p>Phone: {{ $details['phone'] }}</p>
        <p>Comment: {{ $details['comment'] }}</p>
    </div>
</div>

</body>
</html>
