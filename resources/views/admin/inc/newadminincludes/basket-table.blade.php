<table>
    <thead>
    <tr>
        <th> Наименование товара</th>
        <th style="padding: 0 60px">Цена</th>
        <th></th>
        <th></th>
    </tr>
    </thead>


    @csrf
    @foreach($item->products->sort() as $product)
        <tr>
            <td><label><input {{ $product->value ? 'checked' : null }} data-id="{{ $product->id }}" type="checkbox" class="ingredient-enable">{{ $product->title_ru }}</label></td>
            <td style="padding: 0 60px">{{ number_format($product->price, 2, ',', ' ') }} &#8372 </td>
            <td></td>
            <td><input value="{{ $product->value ?? null }}" {{ $product->value ? null : 'disabled' }} data-id="{{ $product->id }}" name="products[{{ $product->id }}]" type="text" class="ingredient-amount form-control" placeholder="Введите количество"></td>
        </tr>
    @endforeach
    <button type="submit" class="btn btn-sm btn-secondary" style="padding: 12px 24px" disabled>Добавить в заказ</button>

</table>
