<nav class="sb-sidenav accordion sb-sidenav-dark text-white" id="sidenavAccordion">
    <div class="sb-sidenav-menu">
        <div class="nav">
            <div class="sb-sidenav-menu-heading">
                <a href="{{ route('admin.dashboard') }}">
                    <span>Админ Панель</span>
                </a>
            </div>
            <ul>
            @can('viewAny', App\Models\User::class)
                <li><a class="{{ Request::path() == 'admin/products' ? 'active' : 'text-white'  }}" href="{{ route('products.index') }}"><i class="fas fa-shopping-bag mx-2"></i> Товары</a></li>
                    <li><a class="{{ Request::path() == 'admin/categories' ? 'active' : 'text-white' }}" href="{{ route('categories.index') }}"><i class="fas fa-list mx-2"></i> Категории</a></li>
                    <li><a class="{{ Request::path() == 'posts' ? 'active' : 'text-white'  }}" href="{{ route('posts.index') }}"><i class="fas fa-paperclip mx-2"></i> Публикации</a></li>
                    <li><a class="{{ Request::path() == 'admin/users' ? 'active' : 'text-white'  }}" href="{{ route('users.index') }}"><i class="fas fa-id-card mx-2"></i> Менеджеры</a></li>
            @endcan
                <li><a class="{{ Request::path() == 'admin/orders' ? 'active' : 'text-white'  }}" href="{{ route('orders.index') }}"><i class="fas fa-cart-arrow-down mx-2"></i> Заказы</a></li>
                <li><a class="{{ Request::path() == 'admin/customers' ? 'active' : 'text-white'  }}" href="{{ route('customers.index') }}"><i class="fas fa-shopping-cart mx-2"></i> Клиенты</a></li>
            </ul>
        </div>
    </div>
    <div class="sb-sidenav-footer">

    </div>
</nav>
