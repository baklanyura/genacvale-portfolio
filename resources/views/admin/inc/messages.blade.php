@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <p class="alert alert-danger m-1">{{ $error }}</p>
    @endforeach
@endif

@if(session()->has('message'))
    <p class="alert alert-success m-1">{{ session('message') }}</p>
@endif
