<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <a class="nav-link" href="{{ route('dashboard') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Адмін Панель
                </a>
                <a class="nav-link" href="{{ route('products.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Товари
                </a>
                <a class="nav-link" href="{{ route('categories') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Категорії
                </a>
                <a class="nav-link" href="{{ route('posts.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Дописи
                </a>
                <a class="nav-link" href="{{ route('orders.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Заказы
                </a>
                <a class="nav-link" href="{{ route('customers.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Магазины
                </a>
                <a class="nav-link" href="{{ route('users.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-user-tie"></i></div>
                    Менеджеры
                </a>
            </div>
        </div>
    </nav>
</div>
