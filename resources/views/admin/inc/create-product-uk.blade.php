<div class="form-group mt-3">
    <label for="title">Найменування</label>
    <input type="text" class="form-control" name="title" id="title" placeholder="Введіть найменування товару" wire:model="title_uk" autofocus>
    @error('title') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
</div>
<div class="form-group">
    <label for="description">Опис</label>
    <textarea class="form-control" name="description" id="description" wire:model="description_uk" placeholder="Введіть опис" rows="5"></textarea>
    @error('description') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
</div>
