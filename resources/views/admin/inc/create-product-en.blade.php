<div class="form-group mt-3">
    <label for="title">Title</label>
    <input type="text" class="form-control" name="title" id="title" placeholder="Enter product title" wire:model="title_en" autofocus>
    @error('title') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
</div>
<div class="form-group">
    <label for="category">Category</label>
    <select wire:model="category_id" class="form-control">
        <option value="" selected>Choose Category</option>
        @foreach($categories as $category)
            <option value="{{ $category->id }}">{{ $category->cat_title_en }}</option>
        @endforeach
    </select>
    @error('category_id') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
</div>
<div class="form-group">
    <label for="image">Image</label>
    <input type="file" class="form-control" name="image" id="image" wire:model="image" placeholder="Enter description">
    @error('image') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
</div>
<div class="form-group">
    <label for="description">Description</label>
    <textarea class="form-control" name="description" id="description" wire:model="description_en" placeholder="Enter description" rows="5"></textarea>
    @error('description') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
</div>
