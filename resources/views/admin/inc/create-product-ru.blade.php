<div class="form-group mt-3">
    <label for="title">Наименование</label>
    <input type="text" class="form-control" name="title" id="title" placeholder="Введите наименование товара" wire:model="title_ru" autofocus>
    @error('title') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
</div>
<div class="form-group">
    <label for="description">Описание</label>
    <textarea class="form-control" name="description" id="description" wire:model="description_ru" placeholder="Введите описание" rows="5"></textarea>
    @error('description') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
</div>
