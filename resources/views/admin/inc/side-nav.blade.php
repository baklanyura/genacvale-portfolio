<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">
                <a  href="{{ route('admin.dashboard') }}">
                   <span>Админ Панель</span>
                </a>
            </li>
            <!-- Optionally, you can add icons to the links -->
            @can('viewAny', App\Models\User::class)
                <li class="{{ Request::path() == 'admin/products' ? 'active' : ''  }}"><a href="{{ route('products.index') }}"><i class="fas fa-shopping-bag"></i> <span>Товары</span></a></li>
                <li class="{{ Request::path() == 'admin/categories' ? 'active' : ''  }}"><a href="{{ route('categories.index') }}"><i class="fas fa-list"></i> <span>Категории</span></a></li>
                <li class="{{ Request::path() == 'posts' ? 'active' : ''  }}"><a href="{{ route('posts.index') }}"><i class="fas fa-pagelines"></i> <span>Публикации</span></a></li>
                <li class="{{ Request::path() == 'admin/users' ? 'active' : ''  }}"><a href="{{ route('users.index') }}"><i class="fas fa-id-card"></i> <span>Менеджеры</span></a></li>
            @endcan
            <li class="{{ Request::path() == 'admin/orders' ? 'active' : ''  }}"><a href="{{ route('orders.index') }}"><i class="fas fa-cart-arrow-down"></i> <span>Заказы</span></a></li>
            <li class="{{ Request::path() == 'admin/customers' ? 'active' : ''  }}"><a href="{{ route('customers.index') }}"><i class="fas fa-shopping-cart"></i> <span>Магазины</span></a></li>

        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
