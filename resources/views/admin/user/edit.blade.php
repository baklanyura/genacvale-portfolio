@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item active"><a href="{{ route('users.index') }}">Менеджеры</a></li>
    <li class="breadcrumb-item active">Редактируем данные менеджера {{ $user->name }}</li>
@endsection

@section('mainContent')
    <!-- Main content -->
    <section class="content mt-5">
        <div class="row">
            <div class="col-md-12">
                <!-- inserted from general form elements file -->
                <div class="card card-primary">
                    <!-- /.card-header -->
                @include('admin.inc.messages')
                <!-- form start -->
                    <form role="form" action="{{ route('users.update', $user) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="name">Имя</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Введите имя" value="{{ $user->name }}">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="email">Электронная почта</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Введите электронную почту" value="{{ $user->email }}">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="phone">Телефон</label>
                                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Введите телефон" value="{{ $user->phone }}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="password">Пароль</label>
                                    <input type="text" class="form-control" id="password" name="password" placeholder="Введите пароль минимум 8 символов" value="{{ old('password') }}">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="role_id">Роль</label>
                                    <select name="role_id" id="role_id" class="form-control">
                                        @foreach($roles as $role)
                                            <option value="{{ $role->id }}" @if($role->id == $user->role_id) selected @endif>{{ $role->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="status_id">Статус</label>
                                    <select name="status_id" id="status_id" class="form-control">
                                        <option value="1" @if($user->status_id == 'Active') selected @endif>Active</option>
                                        <option value="2" @if($user->status_id == 'Inactive') selected @endif>Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer text-center">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                            <a href="{{ route('users.index') }}" class="btn btn-warning">Назад</a>
                        </div>
                    </form>
                </div>
                <!-- /.card -->

            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
@endsection



