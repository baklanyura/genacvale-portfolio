@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item active">Менеджеры</li>
@endsection

@section('mainContent')
    <!-- Main content -->
    <section class="content mt-5">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
    @endif

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                    <a class="offset-lg-5 btn btn-success" href="{{ route('users.create') }}">Создать нового менеджера</a>
            </div>
            <div class="card-body">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Имя</th>
                                    <th>Телефон</th>
                                    <th>Роль</th>
                                    <th>Статус</th>
                                    <th>Эл. почта</th>
                                    <th>Дата</th>
                                    <th>Действия</th>

                                </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $loop->index +1 }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ $user->role->title }}</td>
                                    <td>
                                        <form action="{{ route('admin.changeStatus', $user) }}" method="post" id="statusForm-{{ $user->id }}" style="display: none">
                                            @csrf
                                            @method('PUT')
                                        </form>
                                        <a class="btn @if($user->status->title == 'Active') btn-success @else btn-danger @endif" href="" onclick="
                                            if(confirm('Вы действительно хотите изменить статус этого пользователя?')){
                                            event.preventDefault();
                                            document.getElementById('statusForm-{{ $user->id }}').submit();
                                            }else{
                                            event.preventDefault();
                                            }">{{ $user->status->title }}</a>
                                    </td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->created_at }}</td>
                                    <td><a class="btn btn-success btn-sm" href="{{ route('users.edit', $user) }}"><i class="fas fa-pen-square"></i> </a>

                                        <form action="{{ route('users.destroy', $user->id) }}" method="post" id="deleteForm-{{ $user->id }}" style="display: none">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        <a class="btn btn-danger btn-sm" href="" onclick="
                                            if(confirm('Вы действительно желаете удалить этого пользователя?')){
                                            event.preventDefault();
                                            document.getElementById('deleteForm-{{ $user->id }}').submit();
                                            }else{
                                            event.preventDefault();
                                            }"><i class="fas fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    Footer
                </div>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->
        </div>
    </section>
    <!-- /.content -->
@endsection

@section('admin-footer')
    <!-- DataTables -->
{{--    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>--}}
{{--    <script src="{{ asset('admin./plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>--}}
{{--    <script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>--}}
{{--    <script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>--}}
    <!-- page script -->
    <script>
        // $(document).ready( function () {
        //     $("#example1").DataTable();
        // });
        // $(function () {
        //     $("#example1").DataTable({
        //         "responsive": true,
        //         "autoWidth": false,
        //     });
        //     $('#example2').DataTable({
        //         "paging": true,
        //         "lengthChange": false,
        //         "searching": false,
        //         "ordering": true,
        //         "info": true,
        //         "autoWidth": false,
        //         "responsive": true,
        //     });
        // });

    </script>
@endsection
