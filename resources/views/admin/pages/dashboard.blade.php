{{--@extends('admin.layouts.app')--}}


{{--@section('admin-head')--}}
{{--    <!-- DataTables -->--}}
{{--    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">--}}
{{--    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>--}}
{{--    --}}{{--    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">--}}
{{--    --}}{{--    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">--}}
{{--@endsection--}}
{{--@section('content')--}}

{{--    <div class="container">--}}
{{--        <img src="{{ asset('admin-assets/images/admin-dashboard.png') }}" alt="">--}}
{{--    </div>--}}
{{--@endsection--}}

@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')


@section('mainContent')
    <div class="row">
        <div class="col-xl-6">
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-chart-area mr-1"></i>
                    Area Chart Example
                </div>
                <div class="card-body">
                    <canvas id="myAreaChart" width="100%" height="40"></canvas>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-chart-bar mr-1"></i>
                    Bar Chart Example
                </div>
                <div class="card-body">
                    <canvas id="myBarChart" width="100%" height="40"></canvas>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('newadmin-assets/plugins/chart/Chart.min.js') }}"></script>
    <script src="{{ asset('newadmin-assets/plugins/chart/chart.js') }}"></script>

    @include('admin.inc.newadminincludes.table')

@endsection
