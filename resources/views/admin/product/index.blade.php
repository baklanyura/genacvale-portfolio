@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item active">Товары</li>
@endsection

@section('mainContent')
<div class="card mb-4">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    <div class="card-header text-center">
        <a href="{{ route('products.create') }}" class="btn btn-success">
            Создать новый товар
            <i class="fas fa-plus mr-1"></i>
        </a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped datatable dt-select mt-5" id="example2">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>Наименование</th>
{{--                    <th>Изображение</th>--}}
                    <th>Себестоимость</th>
                    <th>Цена</th>
                    <th>Вес</th>
                    <th>Категория</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
{{--                @dd($products)--}}
                @foreach($products as $item)
                    @if($item->sequence)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->title_ru }}</td>
{{--                        <td><img src="{{ asset('storage/' . $item->image) }}" alt="{{ $item->title }}"--}}
{{--                                 width="40"></td>--}}
                        <td>{{ number_format($item->prime_cost, 2, ',', ' ') }} &#8372</td>
                        <td>{{ number_format($item->price, 2, ',', ' ') }} &#8372</td>
                        <td>{{ $item->weight }} кг</td>
                        <td>{{ $item->category->cat_title_ru }}</td>
                        <td>
                            <a class="btn btn-primary btn-sm mr-0" href="{{ route('products.show', $item) }}">
                                <i class="fas fa-info"></i>
                            </a>

                            <a class="btn btn-success btn-sm" href="{{ route('products.edit', $item) }}"><i
                                    class="fas fa-pen-square"></i></a>

                            <form action="{{ route('products.destroy', $item->id) }}" method="post"
                                  id="deleteForm-{{ $item->id }}" style="display: none">
                                @csrf
                                @method('DELETE')
                            </form>
                            <a class="btn btn-danger btn-sm" href="" onclick="
                                if(confirm('Вы действительно желаете удалить этот товар?')){
                                event.preventDefault();
                                document.getElementById('deleteForm-{{ $item->id }}').submit();
                                }else{
                                event.preventDefault();
                                }"><i class="fas fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <!-- DataTables -->
    {{--    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>--}}
    {{--    <script src="{{ asset('admin./plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>--}}
    {{--    <script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>--}}
    {{--    <script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>--}}
    <!-- page script -->
    <script>
        $(document).ready(function () {
            $("#example2").DataTable({
                "pageLength": 50,
                "ordering": false,
                // "order": [[ 0, 'desc' ]]
            });
        });
    </script>
@endsection
