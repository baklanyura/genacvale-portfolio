@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item"><a href="{{ route('products.index') }}">Товары</a></li>
    <li class="breadcrumb-item active">Редактировать  {{ $product->title_ru }}</li>
@endsection

@section('mainContent')
    <section class="content mt-5">
        <div class="row">
            <div class="col-md-12">
                <!-- inserted from general form elements file -->

                <div class="card card-primary">
                    <form role="form" action="{{ route('products.update', $product) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row mx-1">
                            <div class="form-group mt-3 col-md-4">
                                <label for="title_en">Title</label>
                                <input type="text" class="form-control" name="title_en" id="title_en" placeholder="Enter product title" value="{{ $product->title_en }}" autofocus>
                                @error('title_en') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                            <div class="form-group mt-3 col-md-4">
                                <label for="title_ru">Наименование</label>
                                <input type="text" class="form-control" name="title_ru" id="title_ru" placeholder="Введите наименование товара" value="{{ $product->title_ru }}">
                                @error('title_ru') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                            <div class="form-group mt-3 col-md-4">
                                <label for="title_uk">Найменування</label>
                                <input type="text" class="form-control" name="title_uk" id="title_uk" placeholder="Введіть найменування товару" value="{{ $product->title_uk }}">
                                @error('title_uk') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                        </div>
                        <div class="row mx-1">
                            <div class="form-group col-md-6">
                                <label for="category">Category</label>
                                <select name="category_id" class="form-control">
                                    <option value="" selected>Choose Category</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" @if($category->id == $product->category_id) selected @endif>{{ $category->cat_title_ru }}</option>
                                    @endforeach
                                </select>
                                @error('category_id') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="image">Image</label>
                                <input type="file" class="form-control" name="image" id="image" placeholder="Enter description">
                                @error('image') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                        </div>
                        <div class="row mx-1">
                            <div class="form-group col-md-4">
                                <label for="description_en">Description</label>
                                <textarea class="form-control" name="description_en" id="description_en" placeholder="Enter description" rows="10">{{ $product->description_en }}</textarea>
                                @error('description_en') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label for="description_ru">Описание</label>
                                <textarea class="form-control" name="description_ru" id="description_ru" placeholder="Введите описание" rows="10">{{ $product->description_ru }}</textarea>
                                @error('description_ru') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label for="description_uk">Опис</label>
                                <textarea class="form-control" name="description_uk" id="description_uk" placeholder="Введіть опис" rows="10">{{ $product->description_uk }}</textarea>
                                @error('description_uk') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                        </div>
                        <div class="row mx-1">
                            <div class="form-group mt-3 col-md-2">
                                <label class="text-primary font-weight-bolder" for="barcode">Штрих-код</label>
                                <input type="text" class="form-control" name="barcode" id="barcode" placeholder="Введите штрих-код товара" value="{{ $product->barcode }}">
                                @error('barcode') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                            <div class="form-group mt-3 col-md-2">
                                <label class="text-primary font-weight-bolder" for="trademark">Торговая марка</label>
                                <select class="form-control" name="trademark" id="trademark">
                                    @foreach(\App\Models\Admin\Product::tradeMarks() as $mark)
                                        <option class="form-control"  value="{{ $mark }}" @if($mark == $product->trademark) selected @endif>{{ $mark }}</option>
                                    @endforeach
                                </select>
                                @error('trademark') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                            <div class="form-group mt-3 col-md-2">
                                <label class="text-primary font-weight-bolder" for="prime_cost">Себестоимость</label>
                                <input type="text" class="form-control" name="prime_cost" id="prime_cost"
                                       placeholder="Введите себестоимость товара" value="{{ $product->prime_cost }}">
                                @error('prime_cost') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                            <div class="form-group mt-3 col-md-2">
                                <label class="text-primary font-weight-bolder" for="price">Цена</label>
                                <input type="text" class="form-control" name="price" id="price" placeholder="Введите цену товара" value="{{ $product->price }}">
                                @error('price') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                            <div class="form-group mt-3 col-md-2">
                                <label class="text-primary font-weight-bolder" for="weight">Вес</label><select class="form-control" name="weight" id="weight">
                                    @foreach(\App\Models\Admin\Product::getWeight() as $weight)
                                        <option class="form-control"  value="{{ $weight }}" @if($weight == $product->weight) selected @endif>{{ $weight }}</option>
                                    @endforeach
                                </select>
                                @error('weight') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                            <div class="form-group mt-3 col-md-2">
                                <label class="text-primary font-weight-bolder" for="sequence">Порядок следования</label>
                                <input type="text" class="form-control" name="sequence" id="sequence"
                                       placeholder="Последовательность товаров" value="{{ $product->sequence }}">
                                @error('sequence') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                        </div>
                        <div class="row mx-1">
                            <div class="form-group mt-3 col-md-2">
                                <label class="text-primary font-weight-bolder" for="unit">Единица Товара</label>
                                <input type="text" class="form-control" name="unit" id="unit" placeholder="Введите единицу товара" value="{{ $product->unit }}">
                                @error('unit') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                            <div class="form-group mt-3 col-md-2">
                                <label class="text-primary font-weight-bolder" for="order_unit">Кратность заказа</label>
                                <input type="text" class="form-control" name="order_unit" id="order_unit" placeholder="Введите кратность заказа товара" value="{{ $product->order_unit }}">
                                @error('order_unit') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                            <div class="form-group mt-3 col-md-2">
                                <label class="text-primary font-weight-bolder" for="show_status">Show Status</label>
                                <select class="form-control" name="show_status" id="show_status">
                                    <option class="text-gray" selected disabled>Показывать товар на главной странице</option>
                                    <option value="0" @if($product->show_status == 0) selected @endif>Не показывать</option>
                                    <option value="1" @if($product->show_status == 1) selected @endif>Показывать</option>
                                </select>
                                @error('show_status') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                            <div class="form-group mt-3 col-md-2">
                                <label class="text-primary font-weight-bolder" for="expire">Срок годности</label>
                                <select class="form-control" name="expire" id="expire">
                                    <option value="730" @if($product->expire == 730) selected @endif>730</option>
                                    <option value="365" @if($product->expire == 365) selected @endif>365</option>
                                </select>
{{--                                <input type="text" class="form-control" name="expire" id="expire" placeholder="Введите срок годности товара (в днях)" value="{{ $product->expire }}">--}}
                                @error('expire') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                            <div class="form-group mt-3 col-md-2">
                                <label class="text-primary font-weight-bolder" for="weight_unit">Единица измерения</label>
                                <input type="text" class="form-control" name="weight_unit" id="weight_unit" placeholder="Введите единицу измерения товара" value="{{ $product->weight_unit }}">
                                @error('weight_unit') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                            <div class="form-group mt-3 col-md-2">
                                <label class="text-primary font-weight-bolder" for="qty_box">Количество в ящике</label>
                                <input type="text" class="form-control" name="qty_box" id="qty_box" placeholder="Введите количество единиц товара ящике" value="{{ $product->qty_box }}">
                                @error('qty_box') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                            <a href="{{ route('products.index') }}" class="btn btn-warning">Назад</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
