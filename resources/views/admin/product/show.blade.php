@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item active">Детальная информация о  {{ $product->title_ru }}</li>
@endsection

@section('mainContent')
<div class="card p-2">
    <div class="card-header">
        <div class="row justify-content-around">
            <h3 class="mt-0">Детальная информация - {{ $product->title_ru }}</h3>

        </div>
    </div>
    <div class="card-body">
        <div class="row justify-content-start">
            <img src="{{ asset('storage/' . $product->image) }}" alt="{{ $product->title_ru }}" width="240">
            <div class="d-flex flex-column">
                <h3 class="px-3 text-primary">Наименование</h3>
                <h5 class="px-3">{{ $product->title_en }}</h5>
                <h5 class="px-3">{{ $product->title_uk }}</h5>
            </div>
            <div class="d-flex flex-column">
                <h3 class="px-3 text-primary">Описание</h3>
                <h5 class="px-3">{{ $product->description_ru }}</h5>
                <h5 class="px-3">{{ $product->description_en }}</h5>
                <h5 class="px-3">{{ $product->description_uk }}</h5>
            </div>
        </div>
        <div class="row justify-content-start mt-4">
            <div class="d-flex flex-column col-md-4">
                <h5 class="text-primary">Себестоимость</h5>
                <h3 class="mb-3">{{ number_format($product->prime_cost, 2, ',', ' ') }} &#8372</h3>
            </div>
            <div class="d-flex flex-column col-md-4">
                <h5 class="text-primary">Цена</h5>
                <h3 class="mb-3">{{ number_format($product->price, 2, ',', ' ') }} &#8372</h3>
            </div><div class="d-flex flex-column col-md-4">
                <h5 class="text-primary">Последовательность</h5>
                <h3 class="mb-3">#{{ $product->sequence }}</h3>
            </div>
            <div class="d-flex flex-column col-4">
                <h5 class="text-primary">Категория</h5>
                <h3 class="mb-3">{{ $product->category->cat_title_ru }}</h3>
            </div>
            <div class="d-flex flex-column col-4">
                <h5 class="text-primary">Штрих-код</h5>
                <h3 class="mb-3">{{ $product->barcode }}</h3>
            </div>
            <div class="d-flex flex-column col-4">
                <h5 class="text-primary">Торговая марка</h5>
                <h3 class="mb-3">{{ $product->trademark }}</h3>
            </div>
            <div class="d-flex flex-column col-4">
                <h5 class="text-primary">Срок Годности</h5>
                <h3 class="mb-3">{{ $product->expire }} дней</h3>
            </div>
            <div class="d-flex flex-column col-4">
                <h5 class="text-primary">Единица Товара</h5>
                <h3 class="mb-3">{{ $product->unit }}</h3>
            </div>
            <div class="d-flex flex-column col-4">
                <h5 class="text-primary">Кратность заказа</h5>
                <h3 class="mb-3">{{ $product->order_unit }}</h3>
            </div>
            <div class="d-flex flex-column col-4">
                <h5 class="text-primary">Вес</h5>
                <h3 class="mb-3">{{ $product->weight }}</h3>
            </div>
            <div class="d-flex flex-column col-4">
                <h5 class="text-primary">Единица измерения</h5>
                <h3 class="mb-3">{{ $product->weight_unit }}</h3>
            </div>
           <div class="d-flex flex-column col-4">
                <h5 class="text-primary">Количество единиц товара в
                    ящике</h5>
                <h3 class="mb-3">{{ $product->qty_box }}</h3>
            </div>
        </div>
    </div>
    <div class="card-footer text-center">
        <div class="form-group col-lg-12">
            <div class="col-xs-offset-2 col-xs-8">
                <a type="button" class="btn btn-primary" href="{{ route('products.edit', $product) }}">Редактировать</a>
                <a href="{{ route('products.index') }}" class="btn btn-warning"><i class="fa fa-backward mx-2"></i>Назад</a>
            </div>
        </div>
    </div>
</div>
@endsection
