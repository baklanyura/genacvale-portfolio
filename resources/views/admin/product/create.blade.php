@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item"><a href="{{ route('products.index') }}">Товары</a></li>
    <li class="breadcrumb-item active">Создать новый товар</li>
@endsection

@section('mainContent')
        <div class="card p-2">
            <div class="card-header">
                <div class="row justify-content-around">
                    <h3 class="mt-0">Создать новый товар</h3>
                </div>
            </div>
            <form role="form" action="{{ route('products.store') }}" method="post" enctype="multipart/form-data">
                <div class="card-body">
                    @csrf
                    <div class="row mx-1">
                        <div class="form-group mt-3 col-md-4">
                            <label class="text-primary font-weight-bolder" for="title_en">Title</label>
                            <input type="text" class="form-control" name="title_en" id="title_en"
                                   placeholder="Enter product title" value="{{ old('title_en') }}" autofocus>
                            @error('title_en') <span
                                class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group mt-3 col-md-4">
                            <label class="text-primary font-weight-bolder" for="title_ru">Наименование</label>
                            <input type="text" class="form-control" name="title_ru" id="title_ru"
                                   placeholder="Введите наименование товара" value="{{ old('title_ru') }}">
                            @error('title_ru') <span
                                class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group mt-3 col-md-4">
                            <label class="text-primary font-weight-bolder" for="title_uk">Найменування</label>
                            <input type="text" class="form-control" name="title_uk" id="title_uk"
                                   placeholder="Введіть найменування товару" value="{{ old('title_uk') }}">
                            @error('title_uk') <span
                                class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                    </div>
                    <div class="row mx-1">
                        <div class="form-group col-md-6">
                            <label class="text-primary font-weight-bolder" for="category">Category</label>
                            <select name="category_id" class="form-control">
                                <option value="" selected>Choose Category</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->cat_title_ru }}</option>
                                @endforeach
                            </select>
                            @error('category_id') <span
                                class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label class="text-primary font-weight-bolder" for="image">Image</label>
                            <input type="file" class="form-control" name="image" id="image"
                                   placeholder="Enter description">
                            @error('image') <span
                                class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                    </div>
                    <div class="row mx-1">
                        <div class="form-group col-md-4">
                            <label class="text-primary font-weight-bolder" for="description_en">Description</label>
                            <textarea class="form-control" name="description_en" id="description_en"
                                      placeholder="Enter description" rows="10">{{ old('description_en') }}</textarea>
                            @error('description_en') <span
                                class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label class="text-primary font-weight-bolder" for="description_ru">Описание</label>
                            <textarea class="form-control" name="description_ru" id="description_ru"
                                      placeholder="Введите описание" rows="10">{{ old('description_ru') }}</textarea>
                            @error('description_ru') <span
                                class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label class="text-primary font-weight-bolder" for="description_uk">Опис</label>
                            <textarea class="form-control" name="description_uk" id="description_uk"
                                      placeholder="Введіть опис" rows="10">{{ old('description_uk') }}</textarea>
                            @error('description_uk') <span
                                class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                    </div>
                    <div class="row mx-1">
                        <div class="form-group mt-3 col-md-2">
                            <label class="text-primary font-weight-bolder" for="barcode">Штрих-код</label>
                            <input type="text" class="form-control" name="barcode" id="barcode"
                                   placeholder="Введите штрих-код товара" value="{{ old('barcode') }}">
                            @error('barcode') <span
                                class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group mt-3 col-md-2">
                            <label class="text-primary font-weight-bolder" for="trademark">Торговая марка</label>
                            <select class="form-control" name="trademark" id="trademark">
                                @foreach(\App\Models\Admin\Product::tradeMarks() as $mark)
                                    <option class="form-control"  value="{{ $mark }}">{{ $mark }}</option>
                                @endforeach
                            </select>
                            @error('trademark') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group mt-3 col-md-2">
                            <label class="text-primary font-weight-bolder" for="prime_cost">Себестоимость</label>
                            <input type="text" class="form-control" name="prime_cost" id="prime_cost"
                                   placeholder="Введите себестоимость товара" value="{{ old('prime_cost') }}">
                            @error('prime_cost') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group mt-3 col-md-2">
                            <label class="text-primary font-weight-bolder" for="price">Цена</label>
                            <input type="text" class="form-control" name="price" id="price"
                                   placeholder="Введите цену товара" value="{{ old('price') }}">
                            @error('price') <span
                                class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group mt-3 col-md-2">
                            <label class="text-primary font-weight-bolder" for="weight">Вес</label>
                            <select class="form-control" name="weight" id="weight">
                                <option class="text-gray" selected disabled>Введите вес товара</option>
                                @foreach(\App\Models\Admin\Product::getWeight() as $weight)
                                    <option class="form-control"  value="{{ $weight }}">{{ $weight }}</option>
                                @endforeach
                            </select>
                            @error('weight') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group mt-3 col-md-2">
                            <label class="text-primary font-weight-bolder" for="sequence">Порядок следования</label>
                            <input type="text" class="form-control" name="sequence" id="sequence"
                                   placeholder="Последовательность товаров" value="{{ old('sequence') }}">
                            @error('sequence') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                    </div>
                    <div class="row mx-1">
                        <div class="form-group mt-3 col-md-2">
                            <label class="text-primary font-weight-bolder" for="unit">Единица Товара</label>
                            <input type="text" class="form-control" name="unit" id="unit"
                                   placeholder="Введите единицу товара" value="шт">
                            @error('unit') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group mt-3 col-md-2">
                            <label class="text-primary font-weight-bolder" for="order_unit">Кратность заказа</label>
                            <input type="text" class="form-control" name="order_unit" id="order_unit"
                                   placeholder="Введите кратность заказа товара" value="1">
                            @error('order_unit') <span
                                class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group mt-3 col-md-2">
                            <label class="text-primary font-weight-bolder" for="show_status">Show Status</label>
                            <select class="form-control" name="show_status" id="show_status">
                                <option class="text-gray" selected disabled>Показывать товар на главной странице</option>
                                <option value="0">Не показывать</option>
                                <option value="1">Показывать</option>
                            </select>
                            @error('show_status') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group mt-3 col-md-2">
                            <label class="text-primary font-weight-bolder" for="expire">Срок годности</label>
                            <select class="form-control" name="expire" id="expire">
                                <option value="" selected disabled>Выберите срок годности</option>
                                @foreach(\App\Models\Admin\Product::EXPIRE_TERMS as $term)
                                    <option value="{{ $term }}">{{ $term }}</option>
                                @endforeach
                            </select>
                            @error('expire') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group mt-3 col-md-2">
                            <label class="text-primary font-weight-bolder" for="weight_unit">Единица измерения</label>
                            <input type="text" class="form-control" name="weight_unit" id="weight_unit"
                                   placeholder="Введите единицу измерения товара" value="кг">
                            @error('weight_unit') <span
                                class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                        <div class="form-group mt-3 col-md-2">
                            <label class="text-primary font-weight-bolder" for="qty_box">Количество в ящике</label>
                            <input type="text" class="form-control" name="qty_box" id="qty_box"
                                   placeholder="Введите количество единиц товара ящике" value="70">
                            @error('qty_box') <span
                                class="text-danger error"><small>{{ $message }}</small></span>@enderror
                        </div>
                    </div>
                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary">Создать</button>
                    <a href="{{ route('products.index') }}" class="btn btn-warning">Назад</a>
                </div>
            </form>
        </div>

@endsection
