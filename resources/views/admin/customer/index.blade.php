@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item active">Клиенты</li>
@endsection

@section('mainContent')
    <!-- Main content -->
    <section class="content mt-5">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
    @endif

        <!-- Default box -->
        <div class="card">
{{--            @can('viewAny', \App\Models\User::class)--}}
                <div class="card-header">
                    <a class="btn btn-success" href="{{ route('customers.create') }}">Добавить нового клиента</a>
                    <a class="btn btn-success pull-right" href="{{ route('customers.import.form') }}">Загрузить данные из Excel-файла</a>
                </div>
{{--            @endcan--}}
            <div class="card-body">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Наименование</th>
                                    <th>Адрес</th>
                                    <th>Юр. Наименование</th>
                                    <th>Контактное лицо</th>
                                    <th>Телефон</th>
                                    <th>Действия</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($customers as $customer)
                                <tr>
                                    <td>{{ $customer->title }}</td>
                                    <td>{{ $customer->address }}</td>
                                    <td>{{ $customer->legal_status }}</td>
                                    <td>{{ $customer->contact }}</td>
                                    <td>{{ $customer->phone }}</td>
                                    <td>
                                        <div class="row pl-3">
                                            <a class="btn btn-success btn-sm" href="{{ route('customers.edit', $customer) }}"><i class="fas fa-pen-square"></i></a></p>

                                            <form action="{{ route('customers.destroy', $customer->id) }}" method="post"
                                                  id="deleteForm-{{ $customer->id }}" style="display: none">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                            @can('viewAny', \App\Models\User::class)
                                            <a class="btn btn-danger btn-sm ml-2" href="" onclick="
                                                if(confirm('Вы действительно желаете удалить этот магазин?')){
                                                event.preventDefault();
                                                document.getElementById('deleteForm-{{ $customer->id }}').submit();
                                                }else{
                                                event.preventDefault();
                                                }"><i class="fas fa-trash-alt"></i>
                                            </a>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
                <!-- /.card-body -->

            </div>
            <!-- /.card -->
        </div>
    </section>
    <!-- /.content -->
@endsection

@section('admin-footer')
    <!-- DataTables -->
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin./plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <!-- page script -->
    <script>
        $(document).ready( function () {
            $("#example1").DataTable();
        });
        $(function () {
            $("#example1").DataTable({
                "responsive": true,
                "autoWidth": false,
            });
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
@endsection
