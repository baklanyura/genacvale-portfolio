@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item active"><a href="{{ route('customers.index') }}">Магазины</a></li>
    <li class="breadcrumb-item active">Добавляем магазин</li>
@endsection

@section('mainContent')
    <!-- Main content -->
    <section class="content mt-5">
        <div class="row">
            <div class="col-md-12">
                <!-- inserted from general form elements file -->
                <div class="card card-primary">
                    <!-- /.card-header -->
                @include('admin.inc.messages')
                <!-- form start -->
                    <form role="form" action="{{ route('customers.store') }}" method="post">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="title">Наименование</label>
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Введите наименование" value="{{ old('title') }}">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="address">Адрес</label>
                                    <input type="text" class="form-control" id="address" name="address" placeholder="Введите адрес" value="{{ old('address') }}">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="contact">Контактное лицо</label>
                                    <input type="text" class="form-control" id="contact" name="contact" placeholder="Введите имя контактного лица" value="{{ old('contact') }}">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="phone">Телефон</label>
                                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Введите телефон" value="{{ old('phone') }}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="legal_status">Юридическое Наименование</label>
                                    <input type="text" class="form-control" id="legal_status" name="legal_status" placeholder="Введите юридическое наименование" value="{{ old('legal_status') }}">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="legal_address">Юридический Адрес</label>
                                    <input type="text" class="form-control" id="legal_address" name="legal_address" placeholder="Введите юридический адрес" value="{{ old('legal_address') }}">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="type">Тип Торговой Точки</label>
                                    <select name="type" id="type" class="form-control">
                                        <option selected disabled>Выберите тип торговой точки</option>
                                        <option value="Киоск">Киоск</option>
                                        <option value="Магазин">Магазин</option>
                                        <option value="Супермаркет">Супермаркет</option>
                                        <option value="Пекарня">Пекарня</option>
                                        <option value="Ресторан">Ресторан</option>
                                        <option value="Другое">Другое</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="email">Электронная почта</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Введите электронную почту" value="{{ old('email') }}">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="website">Веб-сайт</label>
                                    <input type="text" class="form-control" id="website" name="website" placeholder="Введите веб-сайт" value="{{ old('website') }}">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer text-center">
                            <button type="submit" class="btn btn-primary">Создать</button>
                            <a href="{{ route('customers.index') }}" class="btn btn-warning">Назад</a>
                        </div>
                    </form>
                </div>
                <!-- /.card -->

            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
@endsection

