@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item active"><a href="{{ route('customers.index') }}">Магазины</a></li>
    <li class="breadcrumb-item active">Редактируем данные магазина {{ $customer->title }}</li>
@endsection

@section('mainContent')
    <!-- Main content -->
    <section class="content mt-5">
        <div class="row">
            <div class="col-md-12">
                <!-- inserted from general form elements file -->
                <div class="card card-primary">
                    <!-- /.card-header -->
                @include('admin.inc.messages')
                <!-- form start -->
                    <form role="form" action="{{ route('customers.update', $customer) }}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="title">Наименование</label>
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Введите наименование" value="{{ $customer->title }}">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="address">Адрес</label>
                                    <input type="text" class="form-control" id="address" name="address" placeholder="Введите адрес" value="{{ $customer->address }}">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="contact">Контактное лицо</label>
                                    <input type="text" class="form-control" id="contact" name="contact" placeholder="Введите имя контактного лица" value="{{ $customer->contact }}">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="phone">Телефон</label>
                                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Введите телефон" value="{{ $customer->phone }}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="legal_status">Юридическое Наименование</label>
                                    <input type="text" class="form-control" id="legal_status" name="legal_status" placeholder="Введите юридическое наименование" value="{{ $customer->legal_status }}">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="legal_address">Юридический Адрес</label>
                                    <input type="text" class="form-control" id="legal_address" name="legal_address" placeholder="Введите юридический адрес" value="{{ $customer->legal_address }}">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="type">Тип Торговой Точки</label>
                                    <select name="type" id="type" class="form-control">
                                        <option selected disabled>Выберите тип торговой точки</option>
                                        <option value="Киоск" @if($customer->type == "Киоск") selected @endif>Киоск</option>
                                        <option value="Магазин" @if($customer->type == "Магазин") selected @endif>Магазин</option>
                                        <option value="Супермаркет" @if($customer->type == "Супермаркет") selected @endif>Супермаркет</option>
                                        <option value="Пекарня" @if($customer->type == "Пекарня") selected @endif>Пекарня</option>
                                        <option value="Ресторан" @if($customer->type == "Ресторан") selected @endif>Ресторан</option>
                                        <option value="Другое" @if($customer->type == "Другое") selected @endif>Другое</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-2">
                                    <label for="email">Электронная почта</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Введите электронную почту" value="{{ $customer->email }}">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="website">Веб-сайт</label>
                                    <input type="text" class="form-control" id="website" name="website" placeholder="Введите веб-сайт" value="{{ $customer->website }}">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer text-center">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                            <a href="{{ route('customers.index') }}" class="btn btn-warning">Назад</a>
                        </div>
                    </form>
                </div>
                <!-- /.card -->

            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
@endsection

