@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item active">Клиенты</li>
@endsection

@section('mainContent')
    <!-- Main content -->
    <section class="content mt-5">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
    @endif

    <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h2>Заполнить базу данных из Excel-файла</h2>
            </div>
            <div class="card-body">
                <div class="card">
                    <form action="{{ route('customers.import.file') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="file">
                        <input type="submit" value="Загрузить" name="submit">
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
