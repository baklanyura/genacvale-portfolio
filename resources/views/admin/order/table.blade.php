<table id="example1" class="table table-bordered ">
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr>
        <td colspan="5"><strong>Накладна № _____</strong></td>
        <td colspan="2" style="text-align: end"><strong>{{ $order->created_at->locale('uk')->isoFormat('LL') }}</strong></td>
        <td colspan="2" style="text-align: end"><strong>__________________</strong></td>
    </tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>

    @if($order->with_account)
        <tr>
            <td colspan="2"><strong>Постачальник: &nbsp;&nbsp;</strong></td>
{{--            <td>{{ $order->customer->legal_status ? $order->customer->legal_status : $order->customer->title }}</td>--}}
            <td>ФОП Приступа А. В.</td>
        </tr>
        <tr>
            <td colspan="2"><strong>Код постачальника: &nbsp;&nbsp;</strong></td>
            <td colspan="2"><span style="float: left">3468304773</span></td>
        </tr>
        <tr>
            <td colspan="2"><strong>Назва Банку: &nbsp;</strong></td>
            <td>РІВНЕНСЬКА ФІЛІЯ АТ КБ "ПРИВАТБАНК"</td>
        </tr>
        <tr>
            <td colspan="2"><strong>Рахунок отримувача в форматі IBAN: &nbsp;&nbsp;</strong></td>
            <td>UA27333391 0000026005054748158</td>
        </tr>
        <tr>
            <td colspan="2"><strong>Валюта:</strong></td>
            <td>UAH</td>
        </tr>
        <tr>
            <td colspan="2"><strong>Одержано: &nbsp;&nbsp;</strong></td>
            <td>@if($order->customer->legal_status) {{ $order->customer->legal_status }}, @endif{{ $order->customer->title }}</td>

        </tr>
        <tr>
            <td colspan="2"></td>
            <td>{{ $order->customer->address }}</td>
        </tr>
    @else
        <tr>
            <td colspan="2"><strong>Постачальник: &nbsp;&nbsp;</strong></td>
            <td><strong>ФОП Приступа А. В.</strong></td>
        </tr>
        <tr>
            <td colspan="2"><strong>Одержано: &nbsp;&nbsp;</strong></td>
            <td colspan="5">@if($order->customer->legal_status) {{ $order->customer->legal_status }}, @endif{{ $order->customer->title }}</td>

        </tr>
        <tr>
            <td colspan="2"></td>
            <td>{{ $order->customer->address }}</td>
        </tr>
    @endif
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <thead>
    <tr>
        @if($order->with_barcode)
            <th style="width: 15px; background-color: #1b4b72; color: white; border: 1px solid white">Штрих-код</th>
        @endif
        <th style="width: 30px; background-color: #1b4b72; color: white; border: 1px solid white">Найменування товару</th>
        <th style="width: 5px; height: 32px; background-color: #1b4b72; color: white; text-align: center; border: 1px solid white" >Од. <br> вим.</th>
        <th align="center" style="width: 7px; background-color: #1b4b72; color: white; border: 1px solid white">Вага<br>у кг</th>
        <th style="width: 7px; background-color: #1b4b72; color: white; border: 1px solid white">Кіл-ть</th>
        <th align="center" style="background-color: #1b4b72; color: white; border: 1px solid white">Ціна</th>
        <th align="center" style="width: 14px; background-color: #1b4b72; color: white; border: 1px solid white">Сума</th>
    </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
        <tr>
            @if($order->with_barcode)
                <td style="border: 1px solid black"><p>`{{ $product->barcode }}</p></td>
            @endif
            <td style="border: 1px solid black"><p>{{ $product->title_ru }}</p></td>
            <td style="border: 1px solid black"><p>{{ $product->unit }}</p></td>
            <td style="border: 1px solid black"><p>{{ $product->weight }}</p></td>
            <td style="text-align: center; border: 1px solid black"><span class="badge">{{ $product->pivot->count }}</span></td>
            <td style="border: 1px solid black">{{ $product->pivot->price ?? $product->price }} грн</td>
            <td style="border: 1px solid black">{{ $product->getPriceForCount() }} грн</td>
        </tr>
    @endforeach
    <tr style="color: red">
        <td colspan="{{ $order->with_barcode ? "6" : "5" }}" style="color: red; border: 1px solid black"><strong>Загальна вартість:</strong></td>
        <td colspan="1" style="text-align: center; color: red; border: 1px solid black"><strong>{{ $order->getFullPrice() }}</strong></td>
    </tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr style="color: red">
        <td colspan="4"><strong>Видпустив: @if($order->with_user){{$order->user->name}}@endif</strong> ________________</td>
        <td colspan="3"><strong>Одержав: &nbsp;</strong> ________________</td>
    </tr>
    </tbody>
</table>
