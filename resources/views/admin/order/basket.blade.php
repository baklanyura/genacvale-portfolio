@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item active"><a href="{{ route('orders.index') }}">Заказы</a></li>
    <li class="breadcrumb-item active"><a href="{{ route('orders.create') }}">Создаем заказ</a></li>
    <li class="breadcrumb-item active">Корзина</li>
@endsection

@section('mainContent')
    <!-- Main content -->
    <section class="content mt-5">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
    @endif

    <!-- Default box -->
        <div class="card">

            <div class="card-body">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered ">
                            <thead>
                            <tr>
                                <th>Наименование</th>
                                <th>Количество</th>
                                <th>Цена</th>
                                <th>Стоимость</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($order->products as $product)
                                <tr>
                                    <td>
                                        <a href="#">
                                            <img src="{{ asset('storage/' . $product->image) }}" alt="{{ $product->title }}" width="40">
                                            {{ $product->title_ru }}
                                        </a>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <form action="{{ route('admin.basket.remove', $product) }}" method="POST">
                                                @csrf
                                                <button type="submit" class="btn btn-danger">
                                                    <span class="fas fa-minus" aria-hidden="true"></span>
                                                </button>
                                            </form>
                                            <span class="badge" style="font-size: 25px;">{{ $product->pivot->count }}</span>
                                            <form action="{{ route('admin.basket.add', $product) }}" method="POST">
                                                @csrf
                                                <button type="submit" class="btn btn-success">
                                                    <span class="fas fa-plus" aria-hidden="true"></span>
                                                </button>
                                            </form>
                                        </div>

                                    </td>
                                    <td>{{ $product->price }} грн</td>
                                    <td>{{ $product->getPriceForCount() }} грн</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="3"><strong class="text-danger">Общая стоимость:</strong></td>
                                <td><strong class="text-danger">{{ $order->getFullPrice() }} грн</strong></td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                    <div class="btn-group pull-right" role="group">
                        <a href="{{ route('orders.create') }}" class="btn btn-warning mr-3">Назад</a>
                        <a type="button" class="btn btn-success" href="{{ route('admin.basket.place') }}" >Оформить заказ</a>
                    </div>
                </div>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->
        </div>
    </section>
    <!-- /.content -->
@endsection
