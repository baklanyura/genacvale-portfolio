@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item active"><a href="{{ route('orders.index') }}">Заказы</a></li>
    <li class="breadcrumb-item active">Редактируем заказ {{ $order->id }}</li>
@endsection

@section('mainContent')
    <!-- Main content -->
    <section class="content mt-5">
        <div class="row">
            <div class="col-md-12">
                <!-- inserted from general form elements file -->
                <div class="card card-primary">
                    <!-- /.card-header -->
                @include('admin.inc.messages')
                <!-- form start -->
                    <form action="{{ route('orders.update', $order) }}" method="POST">
                        @method('PUT')
                        <div class="card-body">
                            <div class="form-group">
                                <label>Клиент</label>
                                <select class="form-control" data-placeholder="Выберите клиента" style="width: 100%;" aria-hidden="true" name="customer_id">
                                    <option selected disabled>Выберите клиента</option>
                                    @foreach($customers as $customer)
                                        <option value="{{ $customer->id }}" {{ $customer->id == $order->customer_id ? 'selected' : '' }}>{{ $customer->title }} | {{ $customer->address }}</option>
                                    @endforeach
                                </select>
                                @error('customer_id')<span class="text-danger"><small>{{ $message }}</small></span>@enderror
                            </div>
                            <div class="d-flex d-inline">
                                <div class="form-check" style="margin: 19px 20px 25px 0">
                                    <input class="form-check-input pt-5" type="checkbox" value="1" id="with_barcode" name="with_barcode" {{ $order->with_barcode ==1 ? 'checked' : '' }}>
                                    <label class="form-check-label" for="with_barcode">Накладная со штрих-кодом</label>
                                </div>
                                <div class="form-check" style="margin-top: 19px; margin-bottom: 25px;">
                                    <input class="form-check-input pt-5" type="checkbox" value="1" id="with_account" name="with_account" {{ $order->with_account ==1 ? 'checked' : '' }}>
                                    <label class="form-check-label" for="with_account">Накладная с банковским счетом</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="payment">
                                    Методы оплаты
                                </label>
{{--                                @dd($order->payment_method)--}}
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="payment_method" id="payment_method" value="1" {{ $order->payment_method == 'Оплата наличными' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="payment_method">
                                        Оплата наличными
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="payment_method" id="payment_method" value="2" {{ $order->payment_method == 'Оплата на карту' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="payment_method">
                                        Оплата на карту
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="payment_method" id="payment_method" value="3" {{ $order->payment_method == 'Оплата на банковский счет' ? 'checked' : '' }}>
                                    <label class="form-check-label" for="payment_method">
                                        Оплата на банковский счет
                                    </label>
                                </div>
                            </div>
                            <table>
                                <thead>
                                <tr>
                                    <th> Наименование товара</th>
                                    <th style="padding: 0 60px">Цена</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                @csrf
                                @foreach($products->sort() as $product)
                                    <tr>
                                        <td><label><input {{ $product->value ? 'checked' : null }} data-id="{{ $product->id }}" type="checkbox" class="ingredient-enable">{{ $product->title_ru }}</label></td>
                                        <td style="padding: 0 60px">{{ number_format($product->price, 2, ',', ' ') }} &#8372 </td>
                                        <td></td>
                                        <td><input value="{{ $product->value ?? null }}" {{ $product->value ? null : 'disabled' }} data-id="{{ $product->id }}" name="products[{{ $product->id }}]" type="text" class="ingredient-amount form-control" placeholder="Введите количество"></td>
                                    </tr>
                                @endforeach
                                <button type="submit" class="btn btn-sm btn-secondary" style="padding: 12px 24px">Редактировать заказ</button>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    @parent
    <script>
        $('document').ready(function () {
            $('.ingredient-enable').on('click', function () {
                let id = $(this).attr('data-id')
                let enabled = $(this).is(":checked")
                $('.ingredient-amount[data-id="' + id + '"]').attr('disabled', !enabled)
                $('.ingredient-amount[data-id="' + id + '"]').val(null)
                // $('.btn-secondary').attr('disabled', !enabled)
            })
        });
    </script>
@endsection


