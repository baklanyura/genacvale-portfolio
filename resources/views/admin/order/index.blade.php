@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item active">Заказы</li>
@endsection

@section('mainContent')
    <!-- Main content -->
    <section class="content mt-5">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
    <!-- Default box -->
        <div class="card">
            <div class="card-header">
                    <a class="offset-lg-5 btn btn-success" href="{{ route('orders.create') }}">Создать новый заказ</a>
            </div>
            <div class="card-body">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped datatable dt-select">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Менеджер</th>
                                <th>Магазин</th>
                                <th>Статус</th>
                                <th>Дата</th>
                                <th>Действия</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td>{{ $order->id }}</td>
                                    <td>{{ $order->user->name }}</td>
                                    <td>{{ $order->customer ? $order->customer->title : "Клиент еще не выбран" }}</td>
                                    <td>{{ $order->status->title }}</td>
                                    <td>{{ $order->created_at }}</td>
                                    <td>
                                        <a class="btn btn-primary btn-sm mr-0" href="{{ route('orders.show', $order) }}">
                                            <i class="fas fa-info px-1"></i>
                                        </a>
                                        <a class="btn btn-success btn-sm" href="{{ route('orders.edit', $order) }}"><i
                                                class="fas fa-pen-square"></i></a>
                                        <form action="{{ route('orders.destroy', $order->id) }}" method="post"
                                              id="deleteForm-{{ $order->id }}" style="display: none">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        <a class="btn btn-danger btn-sm" href="" onclick="
                                            if(confirm('Вы действительно желаете удалить этот заказ?')){
                                            event.preventDefault();
                                            document.getElementById('deleteForm-{{ $order->id }}').submit();
                                            }else{
                                            event.preventDefault();
                                            }"><i class="fas fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.card -->
        </div>
    </section>
    <!-- /.content -->
@endsection

@section('scripts')
    <!-- DataTables -->
    {{--    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>--}}
    {{--    <script src="{{ asset('admin./plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>--}}
    {{--    <script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>--}}
    {{--    <script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>--}}
    <!-- page script -->
    <script>
        $(document).ready(function () {
            $("#example1").DataTable({
                "pageLength": 50,
                "order": [[ 0, 'desc' ]]
            });
            console.log('giorgi');
        });
        // $(function () {
        //     $("#example1").DataTable({
        //         "responsive": true,
        //         "autoWidth": false,
        //     });
        //     $('#example2').DataTable({
        //         "paging": true,
        //         "lengthChange": false,
        //         "searching": false,
        //         "ordering": true,
        //         "info": true,
        //         "autoWidth": false,
        //         "responsive": true,
        //     });
        // });
    </script>
@endsection
