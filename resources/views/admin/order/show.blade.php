@extends('admin.layouts.newApp')
@section('title', 'Заказы')

@section('breadcrumb-item')
    <li class="breadcrumb-item active"><a href="{{ route('orders.index') }}">Заказы</a></li>
    <li class="breadcrumb-item active">Детальная информация о заказе № {{ $order->id }}</li>
@endsection

@section('mainContent')
<div class="container">
    <div class="card p-2">
        <div class="card-header">
            <div class="row justify-content-between">
                <h3 class="mt-0">Детальная информация заказа № {{ $order->id }}</h3>
                <a href="{{ route('orders.export.view', $order) }}" class="btn btn-success">Сохранить заказ в excel формате</a>
                <a href="{{ route('orders.index') }}" class="btn btn-warning"><i class="fa fa-backward mx-2"></i>Назад</a>
            </div>
        </div>
        @if($order->customer)
        <div class="card-body">
            <div class="row justify-content-between">
                <h4 class="ml-2">{{ $order->customer->title }}</h4>
                <h4 class="ml-2">{{ $order->user->name }}</h4>
            </div>
            <h4 class="">{{ $order->customer->address }}</h4>
            <br>
            <h5>Метод оплаты: {{ $order->payment_method }}</h5>
            <table id="example1" class="table table-bordered ">
                <thead>
                <tr>
                    @if($order->with_barcode)
                        <th>Штрих код</th>
                    @endif
                    <th>Наименование</th>
                    <th>Количество</th>
                    <th>Цена</th>
                    <th>Стоимость</th>
                </tr>
                </thead>
                <tbody>
                @foreach($order->products as $product)
                    <tr>
                        @if($order->with_barcode)
                            <td><p>{{ $product->barcode }}</p></td>
                        @endif
                        <td><p>{{ $product->title_ru }}</p></td>
                        <td><span class="badge">{{ $product->pivot->count }}</span></td>
                        <td>{{ $product->pivot->price ?? $product->price }} грн</td>
                        <td>{{ $product->getPriceForCount() }} грн</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="3"><strong class="text-danger">Общая стоимость:</strong></td>
                    <td><strong class="text-danger">{{ $order->getFullPrice() }} грн</strong></td>
                </tr>
                </tbody>
            </table>
            @if($order->comment)
                <h6 class="font-weight-bold">Комментарий:</h6>
                <p>{{ $order->comment }}</p>
            @endif
        </div>
        @else
            <h2 class="text-danger">Заказ не завершен. Предпросмотр невозможен</h2>
        @endif
    </div>
</div>
@endsection
