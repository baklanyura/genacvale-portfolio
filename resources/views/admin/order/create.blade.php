@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item active"><a href="{{ route('orders.index') }}">Заказы</a></li>
    <li class="breadcrumb-item active">Создаем заказ</li>
@endsection

@section('mainContent')
    <!-- Main content -->
    <section class="content mt-5">
        <div class="row">
            <div class="col-md-12">
                <!-- inserted from general form elements file -->
                <div class="card card-primary">
                    <!-- /.card-header -->
                    @include('admin.inc.messages')
                    <div class="card-header">
                        <a class="offset-lg-5 btn btn-success" href="{{ route('admin.basket') }}">В корзину</a>
                    </div>
                    <form action="{{ route('admin.basket.put') }}" method="POST">
                    <div class="card-body">
                        <div class="col-sm-12">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    @php /** @var \App\Models\Category $categories  */ @endphp
                                    @foreach($categories as $key => $item)
                                        <a class="nav-item nav-link @if($loop->first) active @endif"
                                           data-toggle="tab"
                                           href="#id-{{ $loop->iteration }}" role="tab">
                                            {{ $item->cat_title_ru }}
                                        </a>
                                    @endforeach
                                </div>
                            </nav>
                        </div>
                        <div class="tab-content mt-3" id="nav-tabContent">
                            @foreach($categories as $key => $item)
{{--                                @dump($item->products)--}}
                                <div class="tab-pane @if($loop->first) active @endif" id="id-{{ $loop->iteration }}">
                                    @include('admin.inc.newadminincludes.basket-table')
                                </div>
                            @endforeach
                        </div>
                    </div>
                    </form>
                </div>
                <!-- /.card -->

            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
@endsection

@section('scripts')
    @parent
    <script>
        $('document').ready(function () {
            $('.ingredient-enable').on('click', function () {
                let id = $(this).attr('data-id')
                let enabled = $(this).is(":checked")
                $('.ingredient-amount[data-id="' + id + '"]').attr('disabled', !enabled)
                $('.ingredient-amount[data-id="' + id + '"]').val(null)
                $('.btn-secondary').attr('disabled', !enabled)
            })
        });
    </script>
@endsection
