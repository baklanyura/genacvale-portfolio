<!DOCTYPE html>
<html lang="uk">
<head>
    {{--    <meta charset="utf-8" />--}}
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Genacvale</title>
    <link href="{{ asset('admin/css/styles.css') }}" rel="stylesheet" />

{{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="{{ asset('js/app.js') }}" defer></script>
    @livewireStyles
    @yield('admin-head')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
</head>
<body class="sb-nav-fixed">
@include('admin.inc.navbar')
<div id="layoutSidenav">
    @include('admin.inc.side-nav')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Адмін Панель</a></li>
                    <li class="breadcrumb-item active">@yield('breadcrumb-active-page')</li>
                </ol>
{{--                @include('admin.inc.messages')--}}
                @if(isset($slot))
                {{ $slot }}
                @endif
                @yield('content')
            </div>
        </main>

        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="col-md-12">
                    <p>© Copyright 2021 <a href="https://driveteam2020.com/" target="_blank">DriveTeam</a>. All Rights Reserved.</p>
                </div>
            </div>
        </footer>
    </div>
</div>
@livewireScripts
<script type="text/javascript">
    window.livewire.on('itemStore', () => {
        $('#exampleModal').modal('hide');
    });
</script>
<script src="{{ asset('admin/js/scripts.js') }}"></script>
@yield('admin-footer')
</body>
</html>
