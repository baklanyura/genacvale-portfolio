<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Genacvale</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
{{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
{{--    <link href="{{ asset('admin-assets/css/styles.css') }}" rel="stylesheet" />--}}
<!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ asset('admin-assets/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('admin-assets/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin-assets/dist/css/skins/skin-blue.min.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.2/css/jquery.dataTables.css">
{{--    <link rel="stylesheet" href="{{ asset('admin-assets/css/styles.css') }}">--}}
{{--    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />--}}
{{--    <script src="{{ asset('js/app.js') }}" defer></script>--}}
    @livewireStyles
    @yield('admin-head')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    @include('admin.inc.navbar')
    <!-- Left side column. contains the logo and sidebar -->
    @include('admin.inc.side-nav')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Адмін Панель</a></li>
                <li class="breadcrumb-item active">@yield('breadcrumb-active-page')</li>
            </ol>
        </section>
        {{--                @include('admin.inc.messages')--}}
    @if(isset($slot))
        {{ $slot }}
    @endif
    @yield('content')


    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->

        <!-- Default to the left -->
        <p>© Copyright 2021 <a href="https://driveteam2020.com/" target="_blank">DriveTeam</a>. All Rights Reserved.</p>
    </footer>


</div>
<!-- ./wrapper -->




<!-- jQuery 2.2.3 -->
<script src="{{ asset('admin-assets/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('admin-assets/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('admin-assets/dist/js/app.min.js') }}"></script>

<script src="{{ asset('admin-assets/js/scripts.js') }}"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.2/js/jquery.dataTables.js"></script>
@yield('admin-footer')
@livewireScripts
</body>
</html>
