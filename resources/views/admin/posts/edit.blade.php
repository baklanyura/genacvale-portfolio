@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item active"><a href="{{ route('posts.index') }}">Публикации</a></li>
    <li class="breadcrumb-item active">Редактируем публикацию {{ $post->title_ru }}</li>
@endsection

@section('mainContent')
    <!-- Main content -->
    <section class="content mt-5">
        <div class="row">
            <div class="col-md-12">
                <!-- inserted from general form elements file -->
                <div class="card card-primary">
                    <!-- /.card-header -->
                @include('admin.inc.messages')
                <!-- form start -->
                <form role="form" action="{{ route('posts.update', $post->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="title_en">Post Title</label>
                                <input type="text" class="form-control" id="title_en" name="title_en"
                                       placeholder="Enter title" value="{{ $post->title_en }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="title_ru">Заголовок поста</label>
                                <input type="text" class="form-control" id="title_ru" name="title_ru"
                                       placeholder="Введите заголовок" value="{{ $post->title_ru }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="title_uk">Заголовок допису</label>
                                <input type="text" class="form-control" id="title_uk" name="title_uk"
                                       placeholder="Введіть заголовок" value="{{ $post->title_uk }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="subtitle_en">Post Subtitle</label>
                                <input type="text" class="form-control" id="subtitle_en" name="subtitle_en" placeholder="Enter subtitle" value="{{ $post->subtitle_en }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="subtitle_ru">Подзаголовок поста</label>
                                <input type="text" class="form-control" id="subtitle_ru" name="subtitle_ru" placeholder="Введите подзаголовок" value="{{ $post->subtitle_ru }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="subtitle_uk">Підзаголовок допису</label>
                                <input type="text" class="form-control" id="subtitle_uk" name="subtitle_uk" placeholder="Введіть підзаголовок" value="{{ $post->subtitle_uk }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="image">File input</label>
                            <input type='file' name='image' class='form-control'>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card card-outline card-info">

                        <div class="card-body pad">
                            <div class="mb-3">
                                <label for="body_en">Enter recipe</label>
                                <textarea class="textarea" placeholder="Place some text here" name="body_en">{{ $post->body_en }}</textarea>
                            </div>

                            <div class="mb-3">
                                <label for="body_en">Введите рецепт</label>
                                <textarea class="textarea" placeholder="Place some text here" name="body_ru">{{ $post->body_ru }}</textarea>
                            </div>


                            <div class="mb-3">
                                <label for="body_en">Введіть рецепт</label>
                                <textarea class="textarea" placeholder="Place some text here" name="body_uk" >{{ $post->body_uk }}</textarea>
                            </div>
                        </div>
                    </div>
                     @method('PUT')
                    <div class="card-footer text-center">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                        <a href="{{ route('posts.index') }}" class="btn btn-warning">Назад</a>
                    </div>
                    </form>
                </div>
                <!-- /.card -->

            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
@endsection

@section('admin-footer')
    <script src="{{ asset('admin/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace('body_en');
        CKEDITOR.replace('body_ru');
        CKEDITOR.replace('body_uk');
    </script>
@endsection
