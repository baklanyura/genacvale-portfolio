@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item active">Публикации</li>
@endsection

@section('mainContent')
    <!-- Main content -->
    <section class="content mt-5">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
         @endif

    <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <a class="offset-lg-5 btn btn-success" href="{{ route('posts.create') }}">Создать новую публикацию</a>
            </div>
            <div class="card-body">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Название</th>
                                <th>Подзаголовок</th>
                                <th>Slug</th>
                                <th>Дата создания</th>
                                <th>Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    <td>{{ $loop->index +1 }}</td>
                                    <td>{{ $post->title_en }} <br> {{ $post->title_ru }} <br> {{ $post->title_uk }}</td>
                                    <td>{{ $post->subtitle_en }} <br> {{ $post->subtitle_ru }}
                                        <br> {{ $post->subtitle_uk }}</td>
                                    <td>{{ $post->slug }}</td>
                                    <td>{{ $post->created_at }}</td>
                                    <td><a class="btn btn-success btn-sm" href="{{ route('posts.edit', $post->id) }}"><i
                                                class="fas fa-pen-square"></i></a>
                                    <form action="{{ route('posts.destroy', $post->id) }}" method="post"
                                          id="deleteForm-{{ $post->id }}" style="display: none">
                                        @csrf
                                        @method('DELETE')
                                    </form>

                                        <a class="btn btn-danger btn-sm" href="" onclick="
                                            if(confirm('Are you sure, You want to delete this blog?')){
                                            event.preventDefault();
                                            document.getElementById('deleteForm-{{ $post->id }}').submit();
                                            }else{
                                            event.preventDefault();
                                            }"><i class="fas fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
                <!-- /.card-body -->

            </div>
            <!-- /.card -->
        </div>
    </section>
    <!-- /.content -->
@endsection

@section('admin-footer')
    <!-- DataTables -->
    {{--    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>--}}
    {{--    <script src="{{ asset('admin./plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>--}}
    {{--    <script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>--}}
    {{--    <script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>--}}
    <!-- page script -->
    <script>
        $(document).ready(function () {
            $("#example1").DataTable();
        });
        $(function () {
            $("#example1").DataTable({
                "responsive": true,
                "autoWidth": false,
            });
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
@endsection
