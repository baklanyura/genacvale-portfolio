@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item active"><a href="{{ route('posts.index') }}">Публикации</a></li>
    <li class="breadcrumb-item active">Создаем публикацию</li>
@endsection

@section('mainContent')
    <section class="content mt-5">
        <div class="row">
            <div class="col-md-12">
                <!-- inserted from general form elements file -->
                <div class="card card-primary">
                    <!-- /.card-header -->
                @include('admin.inc.messages')
                <!-- form start -->
                    <form role="form" action="{{ route('posts.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="title_en">Post Title</label>
                                    <input type="text" class="form-control" id="title_en" name="title_en"
                                           placeholder="Enter title" value="{{ old('title_en') }}">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="title_ru">Заголовок поста</label>
                                    <input type="text" class="form-control" id="title_ru" name="title_ru"
                                           placeholder="Введите заголовок" value="{{ old('subtitle_ru') }}">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="title_uk">Заголовок допису</label>
                                    <input type="text" class="form-control" id="title_uk" name="title_uk"
                                           placeholder="Введіть заголовок" value="{{ old('subtitle_uk') }}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="subtitle_en">Post Subtitle</label>
                                    <input type="text" class="form-control" id="subtitle_en" name="subtitle_en" placeholder="Enter subtitle" value="{{ old('subtitle_en') }}">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="subtitle_ru">Подзаголовок поста</label>
                                    <input type="text" class="form-control" id="subtitle_ru" name="subtitle_ru" placeholder="Введите подзаголовок" value="{{ old('subtitle_ru') }}">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="subtitle_uk">Підзаголовок допису</label>
                                    <input type="text" class="form-control" id="subtitle_uk" name="subtitle_uk" placeholder="Введіть підзаголовок" value="{{ old('subtitle_uk') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="image">Завантажити картинку</label>
                                <input type='file' name='image' class='form-control'>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card card-outline card-info">

                            <div class="card-body pad">
                                <div class="mb-3">
                                    <label for="body_en">Enter recipe</label>
                                    <textarea class="ckeditor textarea" placeholder="Place some text here" name="body_en" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                </div>

                                <div class="mb-3">
                                    <label for="body_en">Введите рецепт</label>
                                    <textarea class="ckeditor textarea" placeholder="Place some text here" name="body_ru" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                </div>


                                <div class="mb-3">
                                    <label for="body_en">Введіть рецепт</label>
                                    <textarea class="ckeditor textarea" placeholder="Place some text here" name="body_uk" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-center">
                            <button type="submit" class="btn btn-primary">Створити допис</button>
                            <a href="{{ route('posts.index') }}" class="btn btn-warning">Назад</a>
                        </div>
                    </form>
                </div>
                <!-- /.card -->

            </div>
            <!-- /.col-->
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
@endsection

@section('admin-footer')
    <script src="//cdn.ckeditor.com/4.16.0/full/ckeditor.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.ckeditor').ckeditor();
        });
    </script>
@endsection
