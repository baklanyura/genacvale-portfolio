@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item active">Категории</li>
@endsection

@section('mainContent')
    <div class="container">
        <div class="card mb-4">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <div class="card-header text-center">
                <a href="{{ route('categories.create') }}" class="btn btn-success">
                    Создать Категорию
                    <i class="fas fa-plus mr-1"></i>
                </a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered mt-5">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Category Title</th>
                            <th>Назва Категорії</th>
                            <th>Название Категории</th>
                            <th>Slug</th>

                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $value)
                            <tr>
                                <td>{{ $value->id }}</td>
                                <td>{{ $value->cat_title_en }}</td>
                                <td>{{ $value->cat_title_uk }}</td>
                                <td>{{ $value->cat_title_ru }}</td>
                                <td>{{ $value->cat_slug }}</td>
                                <td>
                                    <div class="row pl-3">
                                        <a class="btn btn-success btn-sm ml-2"
                                           href="{{ route('categories.edit', $value) }}"><i
                                                class="fas fa-pen-square"></i></a></p>

                                        <form action="{{ route('categories.destroy', $value->id) }}" method="post"
                                              id="deleteForm-{{ $value->id }}" style="display: none">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        <a class="btn btn-danger btn-sm ml-2" href="" onclick="
                                            if(confirm('Вы действительно желаете удалить эту категорию?')){
                                            event.preventDefault();
                                            document.getElementById('deleteForm-{{ $value->id }}').submit();
                                            }else{
                                            event.preventDefault();
                                            }"><i class="fas fa-trash-alt"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
