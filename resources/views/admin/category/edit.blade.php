@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item active"><a href="{{ route('categories.index') }}">Категории</a></li>
    <li class="breadcrumb-item active">Редактируем категорию {{ $category->cat_title_ru }}</li>
@endsection

@section('mainContent')
<section class="content mt-5">
    <div class="card card-primary">
            <form role="form" action="{{ route('categories.update', $category) }}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group mx-3">
                    <label for="title_en" class="mt-3">Category Title</label>
                    <input type="text" class="form-control" id="title" name="cat_title_en" placeholder="Enter Category Title" value="{{ $category->cat_title_en }}">
                    @error('cat_title') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group mx-3">
                    <label for="title_uk">Назва Категорії</label>
                    <input type="text" class="form-control" id="title_uk" name="cat_title_uk" placeholder="Введіть Назву Категорії" value="{{ $category->cat_title_uk }}">
                    @error('cat_title') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group mx-3">
                    <label for="title_ru">Название Категории</label>
                    <input type="text" class="form-control" id="title_ru" name="cat_title_ru" placeholder="Введите Название Категории" value="{{ $category->cat_title_ru }}">
                    @error('cat_title') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <a href="{{ route('categories.index') }}" class="btn btn-warning">Назад</a>
                </div>
            </form>
    </div>
</section>
@endsection
