@extends('admin.layouts.newApp')
@section('title', 'Админ-панель')

@section('breadcrumb-item')
    <li class="breadcrumb-item active"><a href="{{ route('categories.index') }}">Категории</a></li>
    <li class="breadcrumb-item active">Создаем категорию</li>
@endsection

@section('mainContent')
<section class="content mt-5">
    <div class="card card-primary">
            <form role="form" action="{{ route('categories.store') }}" method="post">
                @csrf
                <div class="form-group mx-3">
                    <label for="title_en" class="mt-3">Category Title</label>
                    <input type="text" class="form-control" id="title" name="cat_title_en" placeholder="Enter Category Title" value="{{ old('cat_title_en') }}">
                    @error('cat_title_en') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group mx-3">
                    <label for="title_uk">Назва Категорії</label>
                    <input type="text" class="form-control" id="title_uk" name="cat_title_uk" placeholder="Введіть Назву Категорії" value="{{ old('cat_title_uk') }}">
                    @error('cat_title_uk') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group mx-3">
                    <label for="title_ru">Название Категории</label>
                    <input type="text" class="form-control" id="title_ru" name="cat_title_ru" placeholder="Введите Название Категории" value="{{ old('cat_title_ru') }}">
                    @error('cat_title_ru') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary">Создать</button>
                    <a href="{{ route('categories.index') }}" class="btn btn-warning">Назад</a>
                </div>
            </form>
    </div>
</section>
@endsection
