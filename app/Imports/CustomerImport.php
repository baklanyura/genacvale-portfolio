<?php

namespace App\Imports;

use App\Models\Admin\Customer;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CustomerImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Customer([
            'title' => $row['title'],
            'address' => $row['address'],
            'legal_status' => $row['legal_status'],
            'legal_address' => $row['legal_address'],
            'contact' => $row['contact'],
            'phone' => $row['phone'],
            'type' => $row['type'],
            'user_id' => $row['user_id'],
            'email' => $row['email'],
            'website' => $row['website'],

        ]);
    }
}
