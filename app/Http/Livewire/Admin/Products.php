<?php

namespace App\Http\Livewire\Admin;

use App\Models\Admin\Category;
use App\Models\Admin\Product;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithFileUploads;

class Products extends Component
{
    use withFileUploads;

    public $products, $categories, $product_id, $category_id, $image;
    public $title_en, $description_en, $title_ru, $description_ru, $title_uk, $description_uk;
    public $updateMode = false;

    public function render()
    {
        $this->products = Product::all();
        $this->categories = Category::all();
        return view('livewire.admin.products.products')
            ->layout('admin.layouts.app');
    }

    private function resetInputFields(){
        $this->title_en = '';
        $this->title_ru = '';
        $this->title_uk = '';
        $this->description_en = '';
        $this->description_ru = '';
        $this->description_uk = '';
        $this->category_id = '';
        $this->image = '';
    }

    public function store()
    {
//        $validatedData = $this->validate([
//            'title_en' => 'required',
//            'title_ru' => 'required',
//            'title_uk' => 'required',
//            'description_en' => 'required|min:10',
//            'description_ru' => 'required|min:10',
//            'description_uk' => 'required|min:10',
//            'category_id' => 'required',
//            'image' => 'image|max:15360',
//        ]);
////        dd($validatedData);
//        $imageName = $this->image->store('images', 'public');
//        $validatedData['image'] = $imageName;
//        $validatedData['slug'] = Str::slug($validatedData['title_en']);
//
//        Product::create($validatedData);
        session()->flash('message', 'Товар створено успішно.');

        $this->resetInputFields();

        $this->emit('itemStore'); // Close model to using to jquery

    }

    public function edit($id)
    {
        $this->updateMode = true;
        $product = Product::where('id',$id)->first();
        $this->product_id = $id;
        $this->title_en = $product->title_en;
        $this->title_ru = $product->title_ru;
        $this->title_uk = $product->title_uk;
        $this->image = $product->image;
        $this->description_en = $product->description_en;
        $this->description_ru = $product->description_ru;
        $this->description_uk = $product->description_uk;
        $this->category_id = $product->category_id;
    }

    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();
    }

    public function update()
    {
        $validatedData = $this->validate([
            'title_en' => 'required',
            'title_ru' => 'required',
            'title_uk' => 'required',
            'description_en' => 'required|min:10',
            'description_ru' => 'required|min:10',
            'description_uk' => 'required|min:10',
            'category_id' => 'required',
        ]);

        if ($this->product_id) {
            $product = Product::find($this->product_id);



            $product->update([
                'title_en' => $this->title_en,
                'title_ru' => $this->title_ru,
                'title_uk' => $this->title_uk,
                'image' => $this->image,
                'slug' => Str::slug($this->title_en),
                'description_en' => $this->description_en,
                'description_ru' => $this->description_ru,
                'description_uk' => $this->description_uk,
                'category_id' => $this->category_id,
            ]);
            $result = Str::startsWith($this->image, 'images');
            dd($this->image);
            if($result) {
                $product['image'] = $this->image->store('images', 'public');
                dd($product);
                $product->save();
            }

            $this->updateMode = false;
            session()->flash('message', 'Товар оновлено успішно.');
            $this->resetInputFields();

        }
    }

    public function delete($id)
    {
        if ($id) {
            Product::where('id', $id)->delete();
            session()->flash('message', 'Товар видалено успішно.');
        }
    }
}
