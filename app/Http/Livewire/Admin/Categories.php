<?php

namespace App\Http\Livewire\Admin;

use App\Models\Admin\Category;
use Illuminate\Support\Str;
use Livewire\Component;

class Categories extends Component
{
    public $categories, $cat_title_en, $cat_title_ru, $cat_title_uk, $category_id;
    public $updateMode = false;

    public function render()
    {
        $this->categories = Category::all();
        return view('livewire.admin.categories.categories')
            ->layout('admin.layouts.app');
    }

    private function resetInputFields(){
        $this->cat_title_en = '';
        $this->cat_title_ru = '';
        $this->cat_title_uk = '';
    }

    public function store()
    {
        $validatedDate = $this->validate([
            'cat_title_en' => 'required',
            'cat_title_ru' => 'required',
            'cat_title_uk' => 'required',
        ]);
        $validatedDate['cat_slug'] = Str::slug($this->cat_title_en);

        Category::create($validatedDate);

        session()->flash('message', 'Категорію створено успішно.');

        $this->resetInputFields();

        $this->emit('itemStore'); // Close model to using to jquery

    }

    public function edit($id)
    {
        $this->updateMode = true;
        $category = Category::where('id',$id)->first();
        $this->category_id = $id;
        $this->cat_title_en = $category->cat_title_en;
        $this->cat_title_ru = $category->cat_title_ru;
        $this->cat_title_uk = $category->cat_title_uk;
    }

    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();


    }

    public function update()
    {
        $validatedDate = $this->validate([
            'cat_title_en' => 'required',
            'cat_title_ru' => 'required',
            'cat_title_uk' => 'required',
        ]);

        if ($this->category_id) {
            $category = Category::find($this->category_id);
            $category->update([
                'cat_title_en' => $this->cat_title_en,
                'cat_title_ru' => $this->cat_title_ru,
                'cat_title_uk' => $this->cat_title_uk,
                'cat_slug' => Str::slug($this->cat_title_en),
            ]);
            $this->updateMode = false;
            session()->flash('message', 'Категорію успішно оновлено.');
            $this->resetInputFields();

        }
    }

    public function delete($id)
    {
        if($id){
            Category::where('id',$id)->delete();
            session()->flash('message', 'Категорію успішно видалено.');
        }
    }
}
