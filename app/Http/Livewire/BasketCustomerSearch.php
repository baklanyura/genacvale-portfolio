<?php

namespace App\Http\Livewire;

use App\Models\Admin\Customer;
use Livewire\Component;

class BasketCustomerSearch extends Component
{
    public $query;
    public $customers = [];
    public $select;

    public function mount()
    {
        $this->empty();
    }

    public function empty()
    {
        $this->query = '';
        $this->customers = [];
        $this->select = '';
    }

    public function selection()
    {
        $temp = $this->customers->where('id', $this->select)->first();
        if(!empty($temp)) {
            $this->query = $temp->title . ', ' . $temp->address;
        }
        $this->customers = [];
    }

    public function render()
    {
        return view('livewire.basket-customer-search');
    }

    public function updatedQuery()
    {
        $this->customers = Customer::where('title', 'like', '%' . $this->query . '%')
            ->orWhere('address', 'like', '%' . $this->query . '%')
            ->orWhere('legal_status', 'like', '%' . $this->query . '%')
            ->get();
    }
}
