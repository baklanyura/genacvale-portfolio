<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Admin\Category;
use App\Models\Admin\Post;
use App\Models\Admin\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{
    public function index()
    {

        $products = Product::all();
        return view('user.pages.home', ['products'=>$products]);
    }

    public function contact()
    {

        return view('user.pages.contact');
    }

    public function about()
    {

        return view('user.pages.about');
    }

    public function categories()
    {

        return view('user.pages.categories');
    }

    public function showCategory($cat_slug)
    {
        $cat=Category::where('cat_slug', $cat_slug)->first();
        return view('user.pages.showCategory', [
            'category'=>$cat
        ]);
    }

    public function blog()
    {
        $posts = Post::paginate(2);
        return view('user.pages.blog', compact('posts'));
    }

    public function showPost($id) {

        $post = Post::find($id);

        return view('user.pages.showPost', compact('post'));
    }

}
