<?php

namespace App\Http\Controllers;

use App\Http\Requests\MailRequest;
use App\Mail\MailClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class MailController extends Controller
{
    public function send(MailRequest $request)
    {
        $details = [

        'name'=>$request->name,
        'mail'=>$request->mail,
        'subject'=>$request->subject,
        'phone'=>$request->phone,
        'comment'=>$request->comment,
    ];
        Mail::to('anutka.kiska.87@gmail.com')->send(new MailClass($details));

        return back()->with('Message_sent', 'Your message has been sent successfully');
    }
}
