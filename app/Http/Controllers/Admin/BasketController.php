<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Customer;
use App\Models\Admin\Order;
use App\Models\Admin\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BasketController extends Controller
{
    public function basket()
    {
        $orderId = session('orderId');
        $order = new Order();
        if(!is_null($orderId)) {
            $order = Order::findOrFail($orderId);
        }
        return view('admin.order.basket', compact('order'));
    }

    public function basketPlace()
    {
//        $user_ids = User::getUserIds();
//        $customers = Customer::orderBy('title')->get();
//        $filtered_customers = $customers->whereBetween('user_id', $user_ids);
//
//        if(Auth::user()->role_id == 3){
//            $customers = $filtered_customers;
//        }
        if(Auth::user()->role_id == 1) {
            $customers = Customer::orderBy('title')->get();
        } else {
            $customers = Customer::where('user_id', Auth::id())->orderBy('title')->get();
        }
        $orderId = session('orderId');
        if(is_null($orderId)) {
            return back();
        }

        $order = Order::find($orderId);
        return view('admin.order.basket-place', compact('order', 'customers'));
    }

    public function basketConfirm(Request $request)
    {
//        return $request;
        $request->validate([
            'customer_id' => 'required',
            'with_barcode' => '',
            'with_account' => '',
            'with_user' => '',
            'payment_method' => 'required',
            'comment' => '',
        ]);
        $orderId = session('orderId');
        if(is_null($orderId)) {
            return redirect(route('admin.basket'));
        }
        $order = Order::find($orderId);
        $order->user_id = Auth::user()->id;
        $order->customer_id = $request->customer_id;
        $order->status_id = 1;
        if($request->with_barcode) {
           $order->with_barcode = $request->with_barcode;
        } else {
            $order->with_barcode = 0;
        }
        if($request->with_account) {
           $order->with_account = $request->with_account;
        } else {
            $order->with_account = 0;
        }
        if($request->with_user) {
           $order->with_user = $request->with_user;
        } else {
            $order->with_user = 0;
        }
        $order->payment_method = $request->payment_method;
        if($request->comment){
            $order->comment = $request->comment;
        }
        $order->save();
        session()->forget('orderId');
        return redirect(route('orders.index'));
    }

    public function basketPut(Request $request)
    {
        $data = $request->input();
        $orderId = session('orderId');
        if(is_null($orderId)) {
            $order = Order::create([
                'user_id' => Auth::user()->id,
                'customer_id' => 0,
                'status_id' => 1,
            ]);
            session(['orderId' => $order->id]);
        } else {
            $order = Order::find($orderId);
        }
//dd($this->mapProducts($data['products']));
        $order->products()->sync($this->mapProducts($data['products']));

        return redirect()->route('admin.basket.place');
    }

    public function basketAdd(Product $product)
    {
        $orderId = session('orderId');
        if(is_null($orderId)) {
            $order = Order::create([
                'user_id' => Auth::user()->id,
                'customer_id' => 0,
                'status_id' => 1,
            ]);
            session(['orderId' => $order->id]);
        } else {
            $order = Order::find($orderId);
        }

        if($order->products->contains($product->id)) {
            $pivotRow = $order->products()->where('product_id', $product->id)->first()->pivot;
            $pivotRow->count++;
            $pivotRow->update();
        } else {
            $order->products()->attach($product->id);
            $pivotRow = $order->products()->where('product_id', $product->id)->first()->pivot;
            $pivotRow->count = request()->quantity;
            $pivotRow->update();
        }

        return back();
    }

    public function basketRemove(Product $product)
    {
        $orderId = session('orderId');
        if(is_null($orderId)) {
            return redirect(route('admin.basket'));
        }
        $order = Order::find($orderId);

        if($order->products->contains($product->id)) {
            $pivotRow = $order->products()->where('product_id', $product->id)->first()->pivot;
            if($pivotRow->count < 2) {
                $order->products()->detach($product->id);
            } else {
                $pivotRow->count--;
                $pivotRow->update();
            }
        }

        return back();
    }

    private function mapProducts($products)
    {
        return collect($products)->map(function ($i, $key) {
            $product = Product::find($key);
            return ['count' => $i, 'price' => $product->price];
        });
    }

//    public function basketSearch(Request $request)
//    {
////        $term = $request->term;
//        if($request->get('query')){
//            $query = $request->get('query');
//            $customers = Customer::where('title', 'LIKE' , '%' . $query . '%')->get();
//        }
////dd($customers);
////        if(count($customers) == 0){
////            $searchResult[] = "Поиск не дал результатов";
////        } else {
////            foreach ($customers as $key => $value){
////                $searchResult[] = $value;
////            }
////        }
////        return $searchResult;
//        $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
//        foreach($customers as $row){
//            $output .='<li>
//                        <a href="#">' . $row->title . '|' . $row->address . ',' . $row->legal_status . '</a>
//                        <input type="hidden" name="customer_id" value="'. $row->id .'">
//                        </li>';
//        }
//        $output .='</ul>';
//        echo $output;
//    }
}
