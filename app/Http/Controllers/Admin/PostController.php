<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Post;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Str;

class PostController extends BaseController
{
//    public function __construct()
//    {
//        $this->authorize('create', User::class);
//    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $posts = Post::all();
        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
//        $this->authorize('create', User::class);
        return view('admin.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Application|RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title_en' => 'required',
            'title_ru' => 'required',
            'title_uk' => 'required',
            'subtitle_en' => 'required',
            'subtitle_ru' => 'required',
            'subtitle_uk' => 'required',
            'body_en' => 'required',
            'body_ru' => 'required',
            'body_uk' => 'required',
        ]);

        if($request->hasFile('image')){
            $imageName = $request->image->store('posts/images', 'public');
        }

//        Post::create($request->except('files'));
        $post =  new Post();
        $post->title_en = $request->title_en;
        $post->title_ru = $request->title_ru;
        $post->title_uk = $request->title_uk;
        $post->subtitle_en = $request->subtitle_en;
        $post->subtitle_ru = $request->subtitle_ru;
        $post->subtitle_uk = $request->subtitle_uk;
        $post->body_en = $request->body_en;
        $post->body_ru = $request->body_ru;
        $post->body_uk= $request->body_uk;
        $post->slug = Str::slug($request->title_en);
        $post->image = $imageName;
        $post->save();

        return redirect(route('posts.index'))->with('success', 'New post created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $post = Post::find($id);
        return view('admin.posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Application|RedirectResponse|Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title_en' => 'required',
            'title_ru' => 'required',
            'title_uk' => 'required',
            'subtitle_en' => 'required',
            'subtitle_ru' => 'required',
            'subtitle_uk' => 'required',
            'body_en' => 'required',
            'body_ru' => 'required',
            'body_uk' => 'required',
        ]);

        if($request->hasFile('image')){
            $imageName = $request->image->store('public/posts/images');
        }

//        Post::create($request->except('files'));
        $post = Post::find($id);
        $post->title_en = $request->title_en;
        $post->title_ru = $request->title_ru;
        $post->title_uk = $request->title_uk;
        $post->subtitle_en = $request->subtitle_en;
        $post->subtitle_ru = $request->subtitle_ru;
        $post->subtitle_uk = $request->subtitle_uk;
        $post->body_en = $request->body_en;
        $post->body_ru = $request->body_ru;
        $post->body_uk= $request->body_uk;
        $post->slug = Str::slug($request->title_en);
        $post->image = $imageName;
        $post->save();

        return redirect(route('posts.index'))->with('success', 'Post updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $post = Post::find($id)->delete();
        return redirect()->route('posts.index')->with('success', 'Post was deleted');
    }
}
