<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Product;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Str;

class ProductController extends BaseController
{
//    public function __construct()
//    {
//        $this->authorize('create', User::class);
//    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $unsorted = Product::all();
        $sorted = $unsorted->sortBy('sequence');
        $products = $sorted->values()->all();
//        $products = new Product();
//        return $products->country('greece');
        return view('admin.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('admin.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Application|RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        $product = Product::create($this->validateRequest());
        $this->storeImage($product);

        return redirect(route('products.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return Application|Factory|View
     */
    public function show(Product $product)
    {
        return view('admin.product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View
     */
    public function edit(Product $product)
    {
        return view('admin.product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Product $product
     * @return Application|RedirectResponse|Redirector
     */
    public function update(Request $request, Product $product)
    {
        $product->update($this->validateRequest());
        $this->storeImage($product);

        return redirect(route('products.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return RedirectResponse
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return back();
    }

    private function storeImage($product)
    {
        if(request()->has('image')) {
            $product->update([
                'image' => request()->image->store('images', 'public')
            ]);
        }
    }

    private function validateRequest()
    {
        $validatedData = request()->validate([
            'title_en' => 'required',
            'title_ru' => 'required',
            'title_uk' => 'required',
            'description_en' => 'required|min:10',
            'description_ru' => 'required|min:10',
            'description_uk' => 'required|min:10',
            'category_id' => 'required',
            'image' => 'sometimes|image|max:15360',
            'barcode' => 'sometimes',
            'trademark' => 'sometimes',
            'expire' => 'sometimes',
            'unit' => 'sometimes',
            'order_unit' => 'sometimes',
            'weight' => 'sometimes',
            'sequence' => 'nullable|numeric',
            'weight_unit' => 'sometimes',
            'qty_box' => 'sometimes',
            'price' => 'required',
            'prime_cost' => 'sometimes',
            'show_status' => 'sometimes',
        ]);
        $validatedData['slug'] = Str::slug($validatedData['title_en']);

        return $validatedData;
    }
}
