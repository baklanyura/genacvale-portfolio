<?php

namespace App\Http\Controllers\Admin;

use App\Exports\OrderExportView;
use App\Http\Requests\OrderRequest;
use App\Models\Admin\Customer;
use App\Models\Admin\Category;
use App\Models\Admin\Order;
use App\Models\Admin\Product;
use App\Models\User;
use Faker\Provider\Base;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

final class OrderController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $orders = Order::orderBy('id', 'desc')->get();
//        return $orders;
        return view('admin.order.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $user_ids = User::getUserIds();
        $customers = Customer::all();
        $filtered_customers = $customers->whereBetween('user_id', $user_ids);

        if(Auth::user()->role_id == 3){
            $customers = $filtered_customers;
        }
        $products = Product::all();
        $categories = Category::all();
        return view('admin.order.create', compact('products', 'categories', 'customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
//        $user_id = Auth::user()->id;
//        dd($request);

    }

    /**
     * Display the specified resource.
     *
     * @param Order $order
     * @return Response
     */
    public function show(Order $order)
    {
        return view('admin.order.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Order $order
     * @return Application|Factory|View
     */
    public function edit(Order $order)
    {
        $customers = Customer::all();
        $order->load('products');

        $products = Product::get()->map(function($product) use ($order){
            $product->value = data_get($order->products->firstWhere('id', $product->id), 'pivot.count') ?? null;
            return $product;
        });
//        return $products;
        return view('admin.order.edit', compact('order', 'products', 'customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Order $order
     * @return RedirectResponse
     */
    public function update(OrderRequest $request, Order $order)
    {

        $data = $request->validated();

        $order->update($data);
        $order->products()->sync($this->mapProducts($data['products']));

        return redirect()->route('orders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Order $order
     * @return RedirectResponse
     */
    public function destroy(Order $order)
    {
        $order->products()->detach();
        $order->delete();
        session()->forget('orderId');
        return back();
    }

    public function exportView(Order $order)
    {
        $sorted = $order->products->sortBy('sequence');
        return Excel::download(new OrderExportView($order, $sorted->values()->all()), 'order.xlsx');

    }

    private function mapProducts($products)
    {
        return collect($products)->map(function ($i, $key) {
            $product = Product::find($key);
            return ['count' => $i, 'price' => $product->price];
        });
    }
}
