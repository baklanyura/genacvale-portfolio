<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;

final class MainController extends BaseController
{

    public function dashboard(): View
    {
        return view('admin.pages.dashboard');
    }

}
