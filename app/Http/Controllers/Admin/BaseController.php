<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BaseController extends Controller
{
    public $admin;
    public $manager;

    public function __construct()
    {
        $this->middleware('auth');

        if (Auth::user() && auth()->user()->role->slug == 'admin') {
            $this->admin = Auth::user();
        } else if (Auth::user() && auth()->user()->role->slug == 'manager') {
            $this->manager = Auth::user();
        }
    }
}
