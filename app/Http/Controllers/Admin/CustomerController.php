<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Imports\CustomerImport;
use App\Models\Admin\Customer;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class CustomerController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        if(Auth::user()->role_id == 1) {
            $customers = Customer::all();
        } else {
            $customers = Customer::where('user_id', Auth::id())->get();
        }
//        $user_ids = User::getUserIds();
//        $customers = Customer::all();
//        $filtered_customers = $customers->whereBetween('user_id', $user_ids);
//
//        if(Auth::user()->role_id == 3){
//            $customers = $filtered_customers;
//        }
        return view('admin.customer.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
//        $this->authorize('create', User::class);
        return view('admin.customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Application|RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
//        $this->authorize('create', User::class);
        $validatedData = $request->validate([
            'title' => 'required',
            'address' => 'required',
            'legal_status' => '',
            'legal_address' => '',
            'contact' => 'required',
            'phone' => 'required',
            'type' => 'required',
            'email' => '',
            'website' => '',
        ]);
        $validatedData['user_id'] = Auth::id();
        Customer::create($validatedData);
        return redirect(route('customers.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Customer $customer
     * @return Application|Factory|View
     */
    public function edit(Customer $customer)
    {
        return view('admin.customer.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Customer $customer
     * @return Application|RedirectResponse|Redirector
     */
    public function update(Request $request, Customer $customer)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'address' => 'required',
            'legal_status' => '',
            'legal_address' => '',
            'contact' => 'required',
            'phone' => 'required',
            'type' => 'required',
            'email' => '',
            'website' => '',
        ]);
        $validatedData['user_id'] = Auth::id();
        $customer->update($validatedData);
        return redirect(route('customers.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Customer $customer
     * @return RedirectResponse
     */
    public function destroy(Customer $customer)
    {
        $this->authorize('create', User::class);
        $customer->delete();
        return back();
    }

    public function importUploadForm()
    {
        return view('admin.customer.import-excel');
    }

    public function importForm(Request $request)
    {
        Excel::import(new CustomerImport, $request->file);
        return redirect(route('customers.index'))->with('Success', 'Данные загрузились успешно');
    }
}
