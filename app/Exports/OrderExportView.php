<?php

namespace App\Exports;

use App\Models\Admin\Order;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class OrderExportView implements WithColumnFormatting, FromView
{
    private $order;
    private $products;
    /**
     * @param Order $order
     */
    public function __construct(Order $order, $products)
    {
        $this->order = $order;
        $this->products = $products;
//        return $this->order;
    }

    public function view(): View
    {
        return view('admin.order.table', [
            'order' => $this->order,
            'products' => $this->products,
        ]);
    }

    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_GENERAL,
        ];
    }
}
