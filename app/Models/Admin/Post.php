<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravelista\Comments\Comment;
use Laravelista\Comments\Commentable;

class Post extends Model
{
    use HasFactory, Commentable;

}
