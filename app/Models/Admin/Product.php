<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    const EXPIRE_TERMS = [365, 547, 730];

    const COUNTRIES = ['georgia', 'greece'];

    protected $guarded = [];

    public static function tradeMarks()
    {
        return [
            'Гамарджоба Генацвале',
            'Голд Соус',
            'Chikori',
            'Kalimera',
            'ELEON',
        ];
    }

    public static function getWeight()
    {
        return [
            '0.010',
            '0.015',
            '0.020',
            '0.025',
            '0.050',
            '0.100',
            '0.150',
            '0.170 ±10%',
            '0.200',
            '0.250',
            '0.300',
            '0.340',
            '0.370',
            '0.400',
            '0.500',
            '0.600',
            '1.000',

        ];
    }



    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }

    public function country($country)
    {
        return Product::where('country', $country)->get();
    }

    public function getPriceForCount()
    {
        if(!is_null($this->pivot) && !is_null($this->pivot->price)) {
            return $this->pivot->count * $this->pivot->price;
        }elseif(!is_null($this->pivot)){
            return $this->pivot->count * $this->price;
        }
        return $this->price;
    }
}
