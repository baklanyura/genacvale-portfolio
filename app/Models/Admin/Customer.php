<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'address', 'legal_status', 'legal_address', 'contact', 'phone', 'type', 'user_id', 'email', 'website'];
}
