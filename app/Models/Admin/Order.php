<?php

namespace App\Models\Admin;

use App\Models\Status;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Order extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'customer_id', 'status_id'];

    public function getPaymentMethodAttribute($attribute)
    {
        return [
            1 => 'Оплата наличными',
            2 => 'Безналичная оплата',
            3 => 'Без оплаты',
        ][$attribute];
    }

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('count', 'price')->withTimestamps();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class);
    }

    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }

    public function hasStatus(...$statuses): bool
    {
        foreach($statuses as $status) {
            if($this->status()->where('slug', $status)->exists()) {
                return true;
            }
        }
        return false;
    }

    public function getFullPrice()
    {
        $sum = 0;
        foreach ($this->products as $product) {
            $sum +=$product->getPriceForCount();
        }
        return $sum;
    }


}
