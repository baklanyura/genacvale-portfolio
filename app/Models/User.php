<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravelista\Comments\Commenter;

class User extends Authenticatable
{

    use HasFactory, Notifiable, Commenter;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'role_id',
        'status_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin(): bool
    {
        return $this->hasRole('admin');
    }

    public function isManager(): bool
    {
        return $this->hasRole('manager');
    }

    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class);
    }

    public function hasRole(...$roles): bool
    {
        foreach($roles as $role) {
            if($this->role()->where('slug', $role)->exists()) {
                return true;
            }
        }

        return false;
    }

    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class);
    }

    public static function getUserIds ()
    {
        $users = User::where('role_id', 3)->get();
        foreach($users as $user) {
            $user_ids[] = $user->id;
        }
        return $user_ids;
    }

    public function hasStatus(...$statuses): bool
    {
        foreach($statuses as $status) {
            if($this->status()->where('slug', $status)->exists()) {
                return true;
            }
        }
        return false;
    }

}
