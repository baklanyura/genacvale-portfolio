<?php
return [
    'en' => [
        'display' => 'Eng',
        'flag-icon' => 'us'
    ],
    'ru' => [
        'display' => 'Рус',
        'flag-icon' => 'ru'
    ],
    'uk' => [
        'display' => 'Укр',
        'flag-icon' => 'uk'
    ],
];
