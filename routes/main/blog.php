<?php

use App\Http\Controllers\Admin\PostController;
use App\Http\Controllers\User\MainController;
use Illuminate\Support\Facades\Route;

Route::get('/blog', [MainController::class, 'blog'])->name('user.blog');
Route::get('/blog/{id}', [MainController::class, 'showPost'])->name('user.showPost');
Route::resource('posts', PostController::class);
