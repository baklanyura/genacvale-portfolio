<?php


use App\Http\Controllers\MailController;
use App\Http\Controllers\User\MainController;
use Illuminate\Support\Facades\Route;

Route::get('/contact', [MainController::class, 'contact'])->name('user.contact');
Route::post('/contact/send', [MailController::class, 'send'])->name('email.send');
