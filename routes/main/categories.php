<?php

use App\Http\Controllers\User\MainController;
use Illuminate\Support\Facades\Route;

Route::get('/categories', [MainController::class, 'categories'])->name('user.categories');
Route::get('/categories/{cat}', [MainController::class, 'showCategory'])->name('user.showCategory');
