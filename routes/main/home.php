<?php

use App\Http\Controllers\MailController;
use App\Http\Controllers\User\MainController;
use Illuminate\Support\Facades\Route;

Route::get('/home', [MainController::class, 'index'])->name('user.home');

Route::get('/', [MainController::class, 'index'])->name('user.home');
Route::post('/send', [MailController::class, 'send'])->name('user.send');
