<?php

use App\Http\Controllers\User\MainController;
use Illuminate\Support\Facades\Route;

Route::get('/about', [MainController::class, 'about'])->name('user.about');
