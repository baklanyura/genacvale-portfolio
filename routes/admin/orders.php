<?php


use App\Http\Controllers\Admin\BasketController;
use App\Http\Controllers\Admin\OrderController;
use Illuminate\Support\Facades\Route;

Route::resource('/orders', OrderController::class);
Route::get('basket', [BasketController::class, 'basket'])->name('admin.basket');
// последняя страничка перед окончательным оформлением заказа
Route::get('basket/place', [BasketController::class, 'basketPlace'])->name('admin.basket.place');
// оформление заказа
Route::post('basket/place', [BasketController::class, 'basketConfirm'])->name('admin.basket.confirm');

Route::post('basket/put-basket', [BasketController::class, 'basketPut'])->name('admin.basket.put');

Route::post('basket/add/{product}', [BasketController::class, 'basketAdd'])->name('admin.basket.add');
Route::post('basket/remove/{product}', [BasketController::class, 'basketRemove'])->name('admin.basket.remove');

Route::get('orders/export-view/{order}', [OrderController::class, 'exportView'])->name('orders.export.view');

Route::get('basket/place/search', [BasketController::class, 'basketSearch'])->name('admin.basket.search');
