<?php


use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;

Route::put('/users/status/{user}', [UserController::class, 'changeStatus'])->name('admin.changeStatus');
Route::resource('/users', UserController::class);
