<?php


use App\Http\Controllers\Admin\CustomerController;
use Illuminate\Support\Facades\Route;

Route::get('/customers/import-form', [CustomerController::class, 'importUploadForm'])->name('customers.import.form');
Route::post('/customers/import-form', [CustomerController::class, 'importForm'])->name('customers.import.file');
Route::resource('/customers', CustomerController::class);
