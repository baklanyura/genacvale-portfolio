<?php

use App\Http\Controllers\Admin\MainController;

use Illuminate\Support\Facades\Route;


Route::get('/', [MainController::class, 'dashboard'])->name('admin.dashboard');

require 'admin/categories.php';
require 'admin/products.php';
require 'admin/blog.php';
require 'admin/users.php';
require 'admin/orders.php';
require 'admin/customers.php';





