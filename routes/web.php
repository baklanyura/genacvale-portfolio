<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();



Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => '\App\Http\Controllers\User\LanguageController@switchLang']);

require_once 'main/home.php';
require_once 'main/contact.php';
require_once 'main/about.php';
require_once 'main/categories.php';
require_once 'main/blog.php';


