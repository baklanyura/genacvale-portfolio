<?php

namespace Database\Seeders;

use App\Models\Admin\Order;
use App\Models\Status;
use App\Models\User;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [

            [
                'title' => 'Active',
                'related_model' => User::class,
                'slug' => 'active',
            ],

            [
                'title' => 'Non Active',
                'related_model' => User::class,
                'slug' => 'non-active',
            ],
            [
                'title' => 'New',
                'related_model' => Order::class,
                'slug' => 'new',
            ],
            [
                'title' => 'Formed',
                'related_model' => Order::class,
                'slug' => 'formed',
            ],
            [
                'title' => 'Delivered ',
                'related_model' => Order::class,
                'slug' => 'delivered',
            ],
            [
                'title' => 'Finished ',
                'related_model' => Order::class,
                'slug' => 'finished',
            ],
            [
                'title' => 'Awaiting payment',
                'related_model' => Order::class,
                'slug' => 'awaiting-payment',
            ],
            [
                'title' => 'Сanceled',
                'related_model' => Order::class,
                'slug' => 'canceled',
            ],
        ];
        foreach ($statuses as $status) {
            Status::create($status);
        }
    }
}
