<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $today = date("Y-m-d H:i:s");
        $data = [
            [
                'cat_title_en'  => 'Souces',
                'cat_title_ru'  => 'Соусы',
                'cat_title_uk'  => 'Соуси',
                'cat_slug'      => 'souces',
            ],
            [
                'cat_title_en'  => 'Seasoning',
                'cat_title_ru'  => 'Приправы',
                'cat_title_uk'  => 'Приправи',
                'cat_slug'      => 'seasoning',
            ],
            [
                'cat_title_en'  => 'Grocery',
                'cat_title_ru'  => 'Бакалея',
                'cat_title_uk'  => 'Бакалія',
                'cat_slug'      => 'grocery',
            ],
            [
                'cat_title_en'  => 'Soft drinks',
                'cat_title_ru'  => 'Безалкогольные напитки',
                'cat_title_uk'  => 'Безалкогольні напої',
                'cat_slug'      => 'soft-drinks',
            ],
            [
                'cat_title_en'  => 'Alcohol',
                'cat_title_ru'  => 'Алкогольные напитки',
                'cat_title_uk'  => 'Алкогольні напої',
                'cat_slug'      => 'alcohol',
            ],
            [
                'cat_title_en'  => 'Spices',
                'cat_title_ru'  => 'Специи',
                'cat_title_uk'  => 'Спеції',
                'cat_slug'      => 'spices',
            ],
            [
                'cat_title_en'  => 'Semi-processed goods',
                'cat_title_ru'  => 'Полуфабрикаты',
                'cat_title_uk'  => 'Напівфабрикати',
                'cat_slug'      => 'semi-processed-goods',
            ],
            [
                'cat_title_en'  => 'Flour and cereals',
                'cat_title_ru'  => 'Мука и крупа',
                'cat_title_uk'  => 'Борошно і крупа',
                'cat_slug'      => 'flour-and-cereals',
            ],
            [
                'cat_title_en'  => 'Georgian Sweets',
                'cat_title_ru'  => 'Сладости',
                'cat_title_uk'  => 'Грузинські Солодощі',
                'cat_slug'      => 'georgian-sweets',
            ],

        ];
        DB::table('categories')->insert($data);

    }
}
