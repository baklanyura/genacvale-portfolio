<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLegalStatusToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {

            $table->string('email')->nullable()->change();
            $table->string('website')->nullable()->change();
            $table->string('legal_status')->after('title')->default('N/A');
            $table->string('legal_address')->after('legal_status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('email')->default('N/A')->change();
            $table->string('website')->default('N/A')->change();
            $table->dropColumn('legal_status');
            $table->dropColumn('legal_address');
        });
    }
}
