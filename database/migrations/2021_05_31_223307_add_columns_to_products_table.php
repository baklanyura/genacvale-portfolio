<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('barcode')->after('category_id');
            $table->string('trademark')->after('barcode');
            $table->unsignedInteger('expire')->after('trademark')->default(730);
            $table->string('unit')->after('expire')->default('шт');
            $table->unsignedInteger('order_unit')->after('unit')->default(1);
            $table->unsignedDouble('weight', 5, 3)->after('order_unit');
            $table->string('weight_unit')->after('weight')->default('кг');
            $table->unsignedInteger('qty_box')->after('weight_unit');
            $table->unsignedDouble('price', 7, 2)->after('qty_box');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('barcode');
            $table->dropColumn('trademark');
            $table->dropColumn('expire');
            $table->dropColumn('unit');
            $table->dropColumn('order_unit');
            $table->dropColumn('weight');
            $table->dropColumn('weight_unit');
            $table->dropColumn('qty_box');
            $table->dropColumn('price');
        });
    }
}
