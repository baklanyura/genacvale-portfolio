<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('two_factor_secret');
            $table->dropColumn('two_factor_recovery_codes');
            $table->string('phone')->default('N/A')->after('email');
            $table->enum('role', ['Admin', 'Manager'])->default('manager')->after('phone');
            $table->enum('status', ['Active', 'Inactive'])->default('Inactive')->after('role');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('two_factor_secret')->nullable()->after('password');
            $table->text('two_factor_recovery_codes')->nullable()->after('password');
            $table->dropColumn('phone');
            $table->dropColumn('role');
            $table->dropColumn('status');
        });
    }
}
