<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title_en', 191);
            $table->string('title_ru', 191);
            $table->string('title_uk', 191);
            $table->string('subtitle_en', 191);
            $table->string('subtitle_ru', 191);
            $table->string('subtitle_uk', 191);
            $table->string('slug', 191);
            $table->mediumText('body_en');
            $table->mediumText('body_ru');
            $table->mediumText('body_uk');
            $table->string('image', 191);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
