<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NewUpdateToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('role');
            $table->dropColumn('status');
            $table->integer('role_id')->unsigned()->nullable()->after('phone');
            $table->integer('status_id')->unsigned()->nullable()->after('role');

            $table->foreign('role_id')
                ->references('id')
                ->on('roles')
                ->onDelete('set null');

            $table->foreign('status_id')
                ->references('id')
                ->on('statuses')
                ->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

            Schema::table('users', function (Blueprint $table) {
                $table->enum('role', ['Admin', 'Manager'])->default('manager')->after('phone');
                $table->enum('status', ['Active', 'Inactive'])->default('Inactive')->after('role');
                $table->dropColumn('role_id');
                $table->dropColumn('status_id');

            });

    }
}
